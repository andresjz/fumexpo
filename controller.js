    var listApp = angular.module('indexEventos', ['monospaced.qrcode']);    
    //var url_base = "http://127.0.0.1:8080/recursoseducativos/coursat/php/CRUDusuario/";
    //var url_base = "http://gineralor.com/trainingcloud.com.mx/php/CRUDcomentarios/index.php";
    var url_base = mainUrl;
    var mensaje = 0;

    listApp.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
    });
    /* variable listApp is  a variable which used to control the array values to show the data to show in view  using the module name 'listApp' with arguments as an array */

    /* Initialize the controller by name 'PhoneListCtrl' holds the information of phone in form of array with keys name, snipper, price , quantity */

    /* $scope argument passed in function is a key arguments should be passed with exactly the same name */



listApp.directive('capitalize', function() {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
      }
    };
});


listApp.controller('administrador', function ($scope, $interval, $http) {

    $scope.filteredUsers =  [];
    $scope.groupedUsers  =  [];
    $scope.UsersPerPage  =  3;
    $scope.pagedUsers    =  [];
    $scope.currentPage   =  0;

    $scope.estadoMensaje = 0;
    var events;


    $interval( function(){ $scope.revisarCambios(); }, 1000 );

    $scope.revisarCambios = function(){ 
        $scope.estadoMensaje = mensaje; 
    }

   $scope.reset_reg_form = function(){
        document.getElementById("regForm").reset();
        $scope.regForm.$setPristine();
   };

   $scope.reset_edit_form = function(){
        document.getElementById("editForm").reset();
        $scope.editForm.$setPristine();     
   };

    /** function to get detail of comment added in mysql referencing php **/

    $scope.get_users = function(){
    $http.get(url_base +"User/Get").success(function(data)
    {
        console.log(data);
        $scope.pagedUsers = data;    
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 5; //max no of Users to display in a page
        $scope.filteredUsers = $scope.pagedUsers.length; //Initially for no filter  
        $scope.totalUsers = $scope.pagedUsers.length;

    })
    .error(function(data, status, headers, config){
            //alert(data);
    })
    ;
    }
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredUsers = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };

    $scope.login = function() {
        mensaje = 4;
        $http.post(url_base + 'Login', 
            {
              "Username": $scope.user,
              "Password": $scope.pass
            }
        )      
        .success(function (data, status, headers, config) {               
            if( data == null ){ 
               alert("Nombre de usuario y / o contraseña incorreacta");
               mensaje = 1;
           } else{
                var id    = data["id"];
                var name  = data["firstname"];
                var email = $scope.user;
                var role  = data["user"]["role"]["name"];
                var pass  = data["user"]["ticket"]["name"];
                pass = pass.replace(/\s/g, '');
                //alert(pass);
                //alert("Inicio de sesión correcta");
                var url = "http://fumexpo.azurewebsites.net/funciones.php?session=&id="+id+"&name="+name+"&email="+email+"&role="+role+"&pass="+pass;
                //var url = "http://localhost/web3/funciones.php?session=&id="+id+"&name="+name+"&email="+email+"&role="+role+"&pass="+pass;
                window.location.href = url;
            }
            
        })

        .error(function(data, status, headers, config){
           alert("Nombre de usuario y / o contraseña incorreacta");
            mensaje = 1;
        });
    };

    /** function to add details for comentario in mysql referecing php **/

    $scope.add_users = function(){
        mensaje = 4;
        if ($scope.pass == 8950 ) {
            $scope.passName = "PASE TIPO A"; 
        } else{
            $scope.extra = 0;
            if ($scope.pass == 3200 ) $scope.passName = "PASE TIPO B"; 
            if ($scope.pass == 150 ) $scope.passName = "PASE TIPO C"; 
        };

        $http.post(url_base + 'Register', 
            {
                  "id": null,
                  "firstname":  $scope.names,
                  "lastname":   $scope.surname,
                  "photo":      "string",
                  "user": {
                        "username": $scope.email,
                        "password": $scope.password,
                        "status":   true,  //Cuando se valide su pago será true
                        "invited_by": $scope.invited_by,
                        "certificate": false,
                        "company": {
                              "name":    $scope.company,
                              "country": $scope.country,
                              "state":   $scope.state,
                              "city":    $scope.city,
                              "phone":   $scope.phone,
                              "mobile":  $scope.celular
                            },
                        "role": {
                          "id":          01,
                          "name":        "Usuario",
                          "description": "Participante del evento"
                        },
                    "ticket": {
                          "id":             null,
                          "name":           $scope.passName,
                          "description":    null,
                          "price":          $scope.pass,
                          "additional_tickets": [
                            {
                              "quantity":   1,
                              "price":      $scope.extra
                            }
                          ]
                    },
                    "events": null
                  }
                }
        )
        .success(function (data, status, headers, config) {
            //location.reload();
            mensaje = 1;
        })
        .error(function(data, status, headers, config){

            var name  = $scope.names+"-"+$scope.surname+"_"+$scope.state+"_"+$scope.phone;
            var email = $scope.email;
            var pass  = $scope.pass;
            var passn = $scope.passName;
            var extra = $scope.extra;

            name  = name.replace(/\s/g, '-');
            passn  = passn.replace(/\s/g, '-');
            
            var names = $scope.names;
            names  = names.replace(/\s/g, '-');

            var surname = $scope.surname;
            surname  = surname.replace(/\s/g, '-');


            var urlSql = "http://fumexpo.azurewebsites.net/SQL/index.php?action=register_user&firstname="+names+"&lastname="+surname+"&passName="+passn+"&company="+$scope.company+"&pass="+pass+"&extra="+extra+"&username="+email+"&phone="+$scope.phone+"&celular="+$scope.celular+"&payment="+$scope.payment;
            alert(urlSql);
             $http.get(urlSql)
        .success(function (data, status, headers, config) {
            var url = "http://fumexpo.azurewebsites.net/funciones.php?mail=&name="+name+"&email="+email+"&pass="+pass+"&passn="+passn+"&extra="+extra;
            //alert(url);
            // var url = "http://fumexpo.azurewebsites.net/web3/funciones.php?mail=&name="+name+"&email="+email+"&pass="+pass+"&passn="+passn+"&extra="+extra;
            window.location.href = url;
            //location.reload();
            // mensaje = 1;
        })


        });        
    }

    /** function to delete comentario from list of product referencing php **/
    /*
    $scope.delete_users = function(index) {  
     var x = confirm("Estas seguro que deseas borrar el registro del usuario");
     if(x){
      $http.delete(url_base + 'User/Delete/'+index, 
            {
                'id'     : index
            }
        )      
        .success(function (data, status, headers, config) {               
            //$scope.get_comentarios();
            alert("El registro ha sido borrado exitosamente");
            //location.reload();
        })

        .error(function(data, status, headers, config){
           alert("El registro ha sido borrado exitosamente");
        });
      }else{

      }
    }*/

    $scope.delete_users = function(index) {  
     var x = confirm("Estas seguro que desea cancelar este pase?");
     if(x){
      $http.put(url_base + 'User/Put/'+index, 
            {
              "id":         index,
              "firstname":  "Cancelado",
              "user":       {status: true}
            }
        )      
        .success(function (data, status, headers, config) {               
            //$scope.get_comentarios();
            alert("El registro ha sido borrado exitosamente");
            //location.reload();
        })

        .error(function(data, status, headers, config){
           alert("El registro ha sido borrado exitosamente");
        });
      }else{

      }
    }


    $scope.validate_users = function(index) {  
      var actual_user, mens;
      //alert("Editar");
      $http.get(url_base + 'User/'+index+ '')      
        .success(function (data, status, headers, config) {    
            actual_user= data;

            actual_user.user.payment  = true;
            if(actual_user.user.ticket.name == "PASE TIPO A"){ mens = index+"-Ahora-puede-elegir-sus-Talleres -"; }
            else{ mens = index; }

            $http.put(url_base + 'User/'+index+'', actual_user)
                .success(function (data, status, headers, config) { 
                    alert("El pago del usuario ha sido actualizado exitosamente");
                    var url = "http://fumexpo.azurewebsites.net/funciones.php?confirmMail=&email="+actual_user.user.username+"&mensaje="+mens;
                    //alert(url);
                    var url = "http://localhost/web3/funciones.php?session=&id="+id+"&name="+name+"&email="+email+"&role="+role+"&pass="+pass;
                    window.location.href = url;
                })
                .error(function(data, status, headers, config){
                   alert("El pago del usuario ha sido actualizado exitosamente");
                   var url = "http://fumexpo.azurewebsites.net/funciones.php?confirmMail=&email="+actual_user.user.username+"&mensaje="+mens;
                   //alert(url);
                    var url = "http://localhost/web3/funciones.php?session=&id="+id+"&name="+name+"&email="+email+"&role="+role+"&pass="+pass;
                    window.location.href = url;
                });


        })

        .error(function(data, status, headers, config){
           alert("error");
        });
    }
    /** fucntion to edit product comment from list of product referencing php **/

    $scope.edit_users = function(index) {  
      //alert("Editar");
      $http.get(url_base + 'User/'+index+ '')      
        .success(function (data, status, headers, config) {    
            //alert(data[0]["id"]);
            $scope.names    = data["firstname"];
            $scope.surname  = data["lastname"];
            $scope.email    = data["user"]["username"];
            $scope.invited_by  = data["user"]["invited_by"];
            $scope.certificate = data["user"]["certificate"];
            $scope.password = data["user"]["password"];
            $scope.payment = data["user"]["payment"];
            $scope.role     = data["user"]["role"]["name"];
            $scope.company  = data["user"]["company"]["name"];
            $scope.country  = data["user"]["company"]["country"];
            $scope.state    = data["user"]["company"]["state"];
            $scope.city     = data["user"]["company"]["city"];
            $scope.phone    = data["user"]["company"]["phone"];
            $scope.celular  = data["user"]["company"]["mobile"];
            $scope.passName = data["user"]["ticket"]["name"];
            $scope.pass     = data["user"]["ticket"]["price"];
            $scope.extra    = data["user"]["ticket"]["additional_tickets"]["0"]["price"];
            $scope.events      = data["user"]["events"];
            events      = data["user"]["events"];
                        
            var role  = data["user"]["role"]["name"];
            var pass  = data["user"]["ticket"]["name"];

            //alert(events);

        })

        .error(function(data, status, headers, config){
           alert("error");
        });
    }

    /** function to update comment details after edit from list of products referencing php **/

    $scope.update_users = function(index) {
        mensaje = 4;
        if ($scope.pass == 8950 ) {
            $scope.passName = "PASE TIPO A"; 
        } else{
            $scope.extra = 0;
            if ($scope.pass == 3200 ) $scope.passName = "PASE TIPO B"; 
            if ($scope.pass == 150 ) $scope.passName = "PASE TIPO C"; 
        };
        //alert(events);
        $http.put(url_base + 'User/'+index+'', 
                    {
                  "id": index,
                  "firstname":  $scope.names,
                  "lastname":   $scope.surname,
                  "photo":      "string",
                  "user": {
                        "username": $scope.email,
                        "password": $scope.password,
                        "status":   true,
                        "payment":  $scope.payment,
                        "invited_by": $scope.invited_by,
                        "certificate": $scope.certificate,  
                        "company": {
                              "name":    $scope.company,
                              "country": $scope.country,
                              "state":   $scope.state,
                              "city":    $scope.city,
                              "phone":   $scope.phone,
                              "mobile":  $scope.celular
                            },
                        "role": {
                          "id":          01,
                          "name":        $scope.role,
                          "description": "Participante del evento"
                        },
                    "ticket": {
                          "id":             null,
                          "name":           $scope.passName,
                          "description":    null,
                          "price":          $scope.pass,
                          "additional_tickets": [
                            {
                              "quantity":   1,
                              "price":      $scope.extra
                            }
                          ]
                    },
                    "events": events
                  }
                }
                  )
                .success(function (data, status, headers, config) { 
                   //alert(data); 
                   alert("Los datos han sido actualizado exitosamente");
                   mensaje = 1;
                   //$scope.get_comentarios();
                })
                .error(function(data, status, headers, config){
                  alert("Los datos han sido actualizado exitosamente");
                   mensaje = 1;
                });
    }

   
});

/*/////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/

listApp.controller('workshops', function ($scope, $interval, $http) {
    var userArray;
    $scope.dia;
    $scope.idWork = "";

    $scope.worksh = "";
    $scope.worksh1 = "";
    $scope.worksh2 = "";
    $scope.tour = "";

    $scope.edit_users = function(index) {  
      //alert("Editar");
      $http.get(url_base + 'User/'+index+ '')      
        .success(function (data, status, headers, config) {    
            //alert(data[0]["id"]);
            $scope.names    = data["firstname"];
            $scope.surname  = data["lastname"];
            $scope.email    = data["user"]["username"];
            $scope.password = data["user"]["password"];
            $scope.company  = data["user"]["company"]["name"];
            $scope.country  = data["user"]["company"]["country"];
            $scope.state    = data["user"]["company"]["state"];
            $scope.city     = data["user"]["company"]["city"];
            $scope.phone    = data["user"]["company"]["phone"];
            $scope.celular  = data["user"]["company"]["mobile"];
            $scope.passName = data["user"]["ticket"]["name"];
            $scope.pass     = data["user"]["ticket"]["price"];
            $scope.extra    = data["user"]["ticket"]["additional_tickets"]["0"]["price"];
            events      = data["user"]["events"];
            $scope.events      = data["user"]["events"];
            $scope.payment      = data["user"]["payment"];
                        
            var role  = data["user"]["role"]["name"];
            var pass  = data["user"]["ticket"]["name"];
            userArray = data;
            //alert(events);
            console.log(data);
        })

        .error(function(data, status, headers, config){
           alert("error");
        });
    }


    $scope.filteredAgenda =  [];
    $scope.groupedAgenda  =  [];
    $scope.AgendaPerPage  =  3;
    $scope.pagedAgenda    =  [];
    $scope.currentPage   =  0;

    $scope.worksh;
    $scope.tour;

    $scope.estadoMensaje = 0;
    var events;
    $scope.workshops = [];

    $scope.get_agenda = function(){
    $http.get(url_base +"Event").success(function(data)
    {
        console.log(data);
      
        // console.log(data[0].activities);

        // data[0].activities.remove(null);        

        // for(var i in data[0].activities){
        //       if(data[0].activities[i] == null ){
        //           delete data[0].activities[i];
                  
        //       }
        // }
        // console.log(data[0].activities);
         $scope.dia = 19;


        $scope.pagedAgenda = data;    
        $scope.currentPage = 1; //current page
        $scope.entryLimit = 5; //max no of Agenda to display in a page
        $scope.filteredAgenda = $scope.pagedAgenda.length; //Initially for no filter  
        $scope.totalAgenda = $scope.pagedAgenda.length;

        
        
        
        
        // for(var i in workshops.activities){
        //   if(value == "" || value == null) {
        //       delete sjonObj.key;
        // }
        //$scope.verificarDisponibilidad();

    })
    .error(function(data, status, headers, config){
            //alert(data);
    })
    ;
    }

    $scope.diaChange = function(){
        alert("CAMBIO");
        $scope.dia++;
    };
    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredAgenda = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };

    $interval( function(){ $scope.revisarCambios(); }, 1000 );
    $interval( function(){ $scope.get_agenda(); }, 500000);

    $scope.verificarDisponibilidad = function(){
        // var ws = document.getElementById('worksh').value;
        // var ws1 = document.getElementById('worksh1').value;
        // var ws2 = document.getElementById('worksh2').value;
        // var tr = document.getElementById('tour').value;

    }

    $scope.revisarCambios = function(){ 
        $scope.estadoMensaje = mensaje; 
    }

   $scope.reset_reg_form = function(){
        document.getElementById("regForm").reset();
        $scope.regForm.$setPristine();
   };

   $scope.reset_edit_form = function(){
        document.getElementById("editForm").reset();
        $scope.editForm.$setPristine();     
   };

        /** function to update comment details after edit from list of products referencing php **/

    $scope.register_workshops = function(index) {
        //mensaje = 4;
        var ws = document.getElementById('worksh').value;
        var ws1 = document.getElementById('worksh1').value;
        var ws2 = document.getElementById('worksh2').value;
        var tr = document.getElementById('tour').value;
        
        var rv = JSON.parse(tr);
        var rv1 = JSON.parse(ws);
        var rv2 = JSON.parse(ws1);
        var rv3 = JSON.parse(ws2);

        console.log(rv);

        $scope.update_workshops_counter(rv.id);
        $scope.update_workshops_counter(rv1.id);
        $scope.update_workshops_counter(rv2.id);
        $scope.update_workshops_counter(rv3.id);

        userArray.user.events = [{
                        "id": "1",
                        "name": "Talleres",
                        "starting_date": "2016-10-20T04:00:46.545Z",
                        "ending_date": "2016-10-22T04:23:59.545Z",
                        "activities": null                        
                      }];
        userArray.user.events[0].activities = [rv , rv1, rv2, rv3 ];
        
        console.log(userArray);
        
        $http.put(url_base + 'User/'+index+'', userArray)
                .success(function (data, status, headers, config) { 
                   //alert(data);                
                   //alert("El registro ha sido actualizado exitosamente");
                  alert("Has registrado tus actividades extra correctamente");
                   
                   mensaje = 1;
                   //$scope.get_comentarios();
                })
                .error(function(data, status, headers, config){
                  alert("Has registrado tus actividades extra correctamente");
                  console.log($scope.selecciones);
                });
    }

        $scope.update_workshops_counter = function(index) {
        var workshops = $scope.workshops;
        var id = workshops.id;

        for(var i in workshops.activities){
            if(workshops.activities[i] != null ){
              if(workshops.activities[i].id == index ){
                  //console.log(workshops.activities[i].id);
                  //console.log(workshops.activities[i].name);
                  console.log(workshops.activities[i].quantity);
                  workshops.activities[i].quantity = workshops.activities[i].quantity - 1; 
                  console.log(workshops.activities[i].quantity);

              }
            }
        }
        $http.put(url_base + 'Event/'+id, workshops )      
         .success(function (data, status, headers, config) {               
             //$scope.get_comentarios();
             //alert("El registro ha sido borrado exitosamente");
             //location.reload();
        })

         .error(function(data, status, headers, config){
            //alert("El registro ha sido borrado exitosamente");
         });

    }
    















    /** function to get detail of comment added in mysql referencing php **/

    $scope.get_workshops = function(){
    $http.get(url_base +"Event/Get/1").success(function(data)
    {
        
         $scope.workshops = data;
    })
    .error(function(data, status, headers, config){
            //alert(data);
    })
    ;
    }
   
    /** function to add details for comentario in mysql referecing php **/

    $scope.add_workshops = function(){
        var initD = document.getElementById("initDate").value;
        var initT =  document.getElementById("initTime").value;
        var endD = document.getElementById("endDate").value;
        var endT = document.getElementById("endTime").value;

        initD = $scope.convertDate(initD); 
        endD = $scope.convertDate(endD);
        
        var workshops = $scope.workshops;
        var id = workshops.id;
        var activities = workshops.activities;

        var d = new Date();
        var idNewWorkshop = d.getTime(); 

        initD = initD+"T"+initT+":00.000Z";
        endD = endD+"T"+endT+":00.000Z";

        //alert(initD);
        //alert(endD);
        // for(var i in workshops.activities){
        //     if(workshops.activities[i].id == index ){
        //         console.log(workshops.activities[i].id);
        //         delete workshops.activities[i];
        //     }
        // }
        // var rv = JSON.parse(tr);
        //  var rv1 = JSON.parse(ws);
        var nuevo = [{
                "id": idNewWorkshop,
                "name": $scope.nameW,
                "description": $scope.descriptions,
                "speaker": $scope.speaker,
                "quantity": $scope.quantity,
                "certificate": $scope.certificate,
                "start": initD,
                "end": endD

        }];
         workshops.activities = activities.concat(nuevo);

      console.log(workshops);
      $http.put(url_base + 'Event/'+id, workshops )      
        .success(function (data, status, headers, config) {               
            //$scope.get_comentarios();
            alert("El registro ha sido realizado exitosamente");
            location.reload();
        })

        .error(function(data, status, headers, config){
           alert("El registro ha sido realizado exitosamente");
           location.reload();
        });
    }

    $scope.convertDate = function(usDate) {
      var dateParts = usDate.split(/(\d{1,2})\/(\d{1,2})\/(\d{4})/);
      return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
    }

    /** fucntion to edit product comment from list of product referencing php **/

    $scope.edit_workshops = function(index) {  
        var workshops = $scope.workshops;
        var id = workshops.id;

        for(var i in workshops.activities){
            if(workshops.activities[i] != null ){
              if(workshops.activities[i].id == index ){

                  $scope.idWork       = index;
                  $scope.nameW        = workshops.activities[i].name;
                  $scope.descriptions = workshops.activities[i].description;
                  $scope.speaker      = workshops.activities[i].speaker;
                  $scope.quantity     = workshops.activities[i].quantity;
                  $scope.certificate  = workshops.activities[i].certificate;
                  $scope.initD        = workshops.activities[i].start;
                  $scope.endD         = workshops.activities[i].end;

                  $scope.oper = 2;
              }
            }
        }
    }

    /* Update workshops */
    $scope.update_workshops = function(index) {
        //Talleres
        var workshops = $scope.workshops;
        var id = workshops.id;
        //datos a actualizar
        var initD = document.getElementById("initDate").value;
        var initT =  document.getElementById("initTime").value;
        var endD = document.getElementById("endDate").value;
        var endT = document.getElementById("endTime").value;

        initD = $scope.convertDate(initD); 
        endD = $scope.convertDate(endD);

        initT = ('0' + initT).slice(-5);
        endT = ('0' + endT).slice(-5);
        
        var workshops = $scope.workshops;
        var id = workshops.id;
        var activities = workshops.activities;


        initD = initD+"T"+initT+":00.000Z";
        endD = endD+"T"+endT+":00.000Z";

        //alert(initD);
        //alert(endD);


        for(var i in workshops.activities){
            if(workshops.activities[i] != null ){
              if(workshops.activities[i].id == index ){

                  workshops.activities[i].name        = $scope.nameW;
                  workshops.activities[i].description = $scope.descriptions;
                  workshops.activities[i].speaker     = $scope.speaker;
                  workshops.activities[i].quantity    = $scope.quantity;
                  workshops.activities[i].certificate = $scope.certificate;
                  workshops.activities[i].start       = initD;
                  workshops.activities[i].end         = endD;
              }
            }
        }
        $http.put(url_base + 'Event/'+id, workshops )      
         .success(function (data, status, headers, config) {               
             //$scope.get_comentarios();
             alert("El registro ha sido actualizado exitosamente");
             location.reload();
        })

         .error(function(data, status, headers, config){
            alert("El registro ha sido actualizado exitosamente");
            location.reload(); 
         });

    }


    /** function to delete comentario from list of product referencing php **/

    $scope.delete_workshops = function(index) {  
         var x = confirm("Estas seguro que deseas borrar el registro de taller");
     if(x){
        var workshops = $scope.workshops;
        var id = workshops.id;

        console.log(workshops);

        for(var i in workshops.activities){
            
            if(workshops.activities[i] != null ){
              if(workshops.activities[i].id == index ){
                console.log(workshops.activities[i].id);
                delete workshops.activities[i];
              }
            }
        }

      console.log(workshops);

      $http.put(url_base + 'Event/'+id, workshops )      
        .success(function (data, status, headers, config) {               
            //$scope.get_comentarios();
            alert("El registro ha sido borrado exitosamente");
            location.reload();
        })

        .error(function(data, status, headers, config){
           alert("El registro ha sido borrado exitosamente");
           location.reload(); 
        });
      }else{
          alert("Operación Cancelada");  
      }
    }

    /** function to update comment details after edit from list of products referencing php **/

    $scope.update_users = function(index) {
        mensaje = 4;
        //alert(events);
        $http.put(url_base + 'User/'+index+'', 
                    {
                  "id": index,
                  "firstname":  $scope.names,
                  "lastname":   $scope.surname,
                  "photo":      "string",
                  "user": {
                        "username": $scope.email,
                        "password": $scope.password,
                        "status":   true,  //Cuando se valide su pago será true
                        "company": {
                              "name":    $scope.company,
                              "country": $scope.country,
                              "state":   $scope.state,
                              "city":    $scope.city,
                              "phone":   $scope.phone,
                              "mobile":  $scope.celular
                            },
                        "role": {
                          "id":          01,
                          "name":        "Usuario",
                          "description": "Participante del evento"
                        },
                    "ticket": {
                          "id":             null,
                          "name":           $scope.passName,
                          "description":    null,
                          "price":          $scope.pass,
                          "additional_tickets": [
                            {
                              "quantity":   1,
                              "price":      $scope.extra
                            }
                          ]
                    },
                    "events": events
                  }
                }
                  )
                .success(function (data, status, headers, config) { 
                   //alert(data);                
                   alert("El registro ha sido actualizado exitosamente");
                   mensaje = 1;
                   $scope.get_comentarios();
                })
                .error(function(data, status, headers, config){
                   mensaje = 1;
                });
    }

   
});


listApp.controller('agenda', function ($scope, $interval, $http) {

    $scope.filteredUsers =  [];
    $scope.groupedUsers  =  [];
    $scope.UsersPerPage  =  3;
    $scope.pagedUsers    =  [];
    $scope.currentPage   =  0;

    $scope.estadoMensaje = 0;
    var events;


    $interval( function(){ $scope.revisarCambios(); }, 1000 );

    $scope.revisarCambios = function(){ 
        $scope.estadoMensaje = mensaje; 
    }

   $scope.reset_reg_form = function(){
        document.getElementById("regForm").reset();
        $scope.regForm.$setPristine();
   };

   $scope.reset_edit_form = function(){
        document.getElementById("editForm").reset();
        $scope.editForm.$setPristine();     
   };

    /** function to get detail of comment added in mysql referencing php **/

    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredUsers = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };

     $scope.convertDate = function(usDate) {
      var dateParts = usDate.split(/(\d{1,2})\/(\d{1,2})\/(\d{4})/);
      return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
    }

  
    /** function to add details for comentario in mysql referecing php **/

    $scope.add_agenda = function(){
        alert("entro");
        mensaje = 4;
        var initD = document.getElementById("initDate").value;
        var initT =  document.getElementById("initTime").value;
        var endD = document.getElementById("endDate").value;
        var endT = document.getElementById("endTime").value;

        initD = $scope.convertDate(initD); 
        endD = $scope.convertDate(endD);
        
      
        var d = new Date();
        var idNewWorkshop = d.getTime(); 

        initD = initD+"T"+initT+":00.000Z";
        endD = endD+"T"+endT+":00.000Z";

        $http.post(url_base + 'Event',     
            {
              "id": null,
              "name": $scope.nameE,
              "starting_date": initD,
              "ending_date": endD,
              "activities": [
                {
                  "id": idNewWorkshop,
                  "name": null,
                  "description": $scope.descriptions,
                  "speaker": $scope.speaker,
                  "quantity": $scope.quantity,
                  "certificate": $scope.certificate,
                  "start": null,
                  "end": null
                }
              ]
            }
        )
        .success(function (data, status, headers, config) {
            //location.reload();
            alert("Evento registrado exitosamente");
            mensaje = 1;
        })
        .error(function(data, status, headers, config){
            mensaje = 1;
            alert("Evento registrado exitosamente");
        });        
    };

     $scope.edit_agenda = function(index){
        alert("editar agenda");

     };

     $scope.update_agenda = function(index){
        alert("actualizar agenda");

     };


     $scope.delete_agenda = function(index) {  
     var x = confirm("Estas seguro que deseas borrar el registro del usuario");
     if(x){
      $http.delete(url_base + 'Event/Delete/'+index, 
            {
                'id'     : index
            }
        )      
        .success(function (data, status, headers, config) {               
            //$scope.get_comentarios();
            alert("El registro ha sido borrado exitosamente");
            location.reload();
        })

        .error(function(data, status, headers, config){
           alert("El registro ha sido borrado exitosamente");
           location.reload();
        });
      }else{

      }
    }
   
});

listApp.controller('sponsors', function ($scope, $interval, $http) {

    $scope.filteredUsers =  [];
    $scope.groupedUsers  =  [];
    $scope.UsersPerPage  =  3;
    $scope.pagedUsers    =  [];
    $scope.currentPage   =  0;

    $scope.estadoMensaje = 0;
    var events;


    $interval( function(){ $scope.revisarCambios(); }, 1000 );

    $scope.revisarCambios = function(){ 
        $scope.estadoMensaje = mensaje; 
    }

   $scope.reset_reg_form = function(){
        document.getElementById("regForm").reset();
        $scope.regForm.$setPristine();
   };

   $scope.reset_edit_form = function(){
        document.getElementById("editForm").reset();
        $scope.editForm.$setPristine();     
   };

    /** function to get detail of comment added in mysql referencing php **/

    $scope.setPage = function(pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.filter = function() {
        $timeout(function() { 
            $scope.filteredUsers = $scope.filtered.length;
        }, 10);
    };
    $scope.sort_by = function(predicate) {
        $scope.predicate = predicate;
        $scope.reverse = !$scope.reverse;
    };

     $scope.convertDate = function(usDate) {
      var dateParts = usDate.split(/(\d{1,2})\/(\d{1,2})\/(\d{4})/);
      return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
    }

  
    /** function to add details for comentario in mysql referecing php **/

    $scope.add_sponsors = function(){
        //alert("ENTRO");
        $http.post(url_base + 'Partner',     
            {
              "id":        null,  
              "name":     $scope.nameS,
              "logo":     $scope.logoS,
              "website":  $scope.siteS,
              "phone":    $scope.phoneS,
              "giro":     $scope.giroS,
              "status":     true
            }
        )
        .success(function (data, status, headers, config) {
            //location.reload();
            alert("Patrocinador registrado exitosamente");
            mensaje = 1;
        })
        .error(function(data, status, headers, config){
            mensaje = 1;
            alert("Patrocinador registrado exitosamente");
        });
        document.getElementById("AddForm").reset();        
    };

     $scope.edit_sponsors = function(index){
        $scope.oper = 2;
        //alert("Editar patrocinador");
        $http.get(url_base + 'User/'+index+ '')      
        .success(function (data, status, headers, config) {    
            //alert(data[0]["id"]);
            $scope.names    = data["firstname"];

        })

        .error(function(data, status, headers, config){
           alert("error");
        });

     };

     $scope.update_sponsors = function(index){
        alert("Actualizar patrocinador");

     };


     $scope.delete_sponsors = function(index) {  
     var x = confirm("Estas seguro que deseas borrar el registro del patrocinador");
     if(x){
      $http.delete(url_base + 'Partner/Delete/'+index, 
            {
                'id'     : index
            }
        )      
        .success(function (data, status, headers, config) {               
            //$scope.get_comentarios();
            alert("El registro ha sido borrado exitosamente");
            location.reload();
        })

        .error(function(data, status, headers, config){
           alert("El registro ha sido borrado exitosamente");
           location.reload();
        });
      }else{

      }
    }
   
});