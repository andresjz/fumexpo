<?php
include("config.php");

require ("funciones.php");
//seguridadIndex();
		$error = 0;
		if(isset($_POST['login'])){
			$recordarme=0;

		//if(isset($_POST['recordarme']))$recordarme=1;

		$error = login(limpiar($_POST['user']), $_POST['pass'],$recordarme);

		if($error>0){
			header("Location: dashboard.php");
			exit();
		}

		else if($error == -1){
			echo"<script>alert('Usuario o Clave Incorrecta');</script>";
		}

		/*

		<?php

            switch ($error) {

                case -1://login

                    echo '<br/><strong>Usuario o clave incorrecta</strong>';

                    break;*/		
	}
	if(isset($_POST['recoverPassword'])){
		$email = $_POST['recoverMail'];
		recover($email);
		//echo"<script type='text/javascript'>$(window).load(function(){$('#myModal').modal('show');});</script>";
	}
	if(isset($_POST['searchHeader'])){
		$search = $_POST['searchHeader'];
		
		//echo "<script>alert(".$search.");	</script>";
		
	}


	if(isset($_POST['salir'])){    

		$_SESSION = array();

		//guardar el nombre de la sessión para luego borrar las cookies
		$session_name = session_name();

		//Para destruir una variable en específico
		unset($_SESSION['usuario']);	 

		// Finalmente, destruye la sesión
		session_destroy();

		// Es necesario hacer una petición http para que el navegador las elimine
		if ( isset( $_COOKIE[ $session_name ] ) ) {
			setcookie($session_name, '', time()-3600, '/');   
		}

		if(isset($_COOKIE['identificado'])){
			setcookie('identificado', '', time()-3600, '/'); 
		}

		header("Location: index.php");
		exit();
	}
?>
<!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <a href="index.php" class="logo"><!--span>FUMEXPO<span>2016</span></span></a-->
						<img src="assets/images/fumexpo/FMX_Logo.png" width="150px" alt="logo" style="margin-bottom: 10px;"></a>
						
                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras">
						<?php  if($_SESSION["usuario"]) {?>
                        <ul class="nav navbar-nav navbar-right pull-right">
                            <li>
                                <!-- Notification -->
                                <div class="notification-box">
                                    <ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!-- End Notification bar -->
                            </li>

                            <li class="dropdown user-box">
                                <a href="" class="dropdown-toggle waves-effect waves-light profile " data-toggle="dropdown" aria-expanded="true">
                                    <img src="assets/images/users/user.png" alt="user-img" class="img-circle user-img">
                                    <div class="user-status online"><i class="zmdi zmdi-dot-circle"></i></div>
                                </a>

                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0)"><i class="ti-user m-r-5"></i> Perfil</a></li>
                                    <li>
											<form action="" method="post">
											  <button type="submit" name="salir" value="Cerrar Sesión" class="btn-link">Cerrar Sesión</button>
											</form>
									<!--a href="javascript:void(0)"><i class="ti-power-off m-r-5"></i> Cerrar sesión</a-->
									</li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right pull-right">
                            <li>
                                <a href="dashboard.php">Inicio</a>
                            </li>
						</ul>
						<?php if(  $_SESSION["email"] == "admin@admin.com" ) {?>
						<ul class="nav navbar-nav navbar-right pull-right">
                            <li>
                                <a href="admin.php">Administrador</a>
                            </li>
							</ul>
                        <?php }} else {?>
							<ul class="nav navbar-nav navbar-right pull-right">
                            <li>
                                <a href="index.php">Iniciar Sesión</a>
                            </li>
							</ul>
                        <?php }?>
                    </div>

                </div>
                <?php if($menu == 1){?>
			        <header>
			            <div class="navbar-custom">
			                <div class="container">
			                    <div id="navigation">
			                        <!-- Navigation Menu-->
			                        <ul class="navigation-menu active">
			                            <li>
			                                <form method="POST" id="attendees" action="admin.php">
		                                        <input type="hidden" name="section" value="attendees">
		                                        <a href="javascript:{}" onclick="document.getElementById('attendees').submit();" ><i class="zmdi zmdi-view-dashboard"></i> <span> Participantes </span></a> 
		                                        &nbsp;&nbsp;&nbsp;&nbsp;
		                                    </form>&nbsp;
			                            </li>
			                            <li class="has-submenu">
			                            	<form method="POST" id="agenda" action="admin.php">
		                                        <input type="hidden" name="section" value="agenda">
		                                        <a href="javascript:{}" onclick="document.getElementById('agenda').submit();"></i> <span> Agenda </span></a> 
		                                         &nbsp;&nbsp;&nbsp;&nbsp;
		                                    </form>&nbsp;
			                            </li>

			                            <li class="has-submenu">
			                            	<form method="POST" id="sponsors" action="admin.php">
		                                        <input type="hidden" name="section" value="sponsors">
		                                        <a href="javascript:{}" onclick="document.getElementById('sponsors').submit();" ><i class="zmdi zmdi-collection-text"></i><span> Patrocinadores </span></a> 
		                                         &nbsp;&nbsp;&nbsp;&nbsp;
		                                    </form>&nbsp;
			                            </li>

			                            <li class="has-submenu ">
			                                <form method="POST" id="workshops" action="admin.php">
		                                        <input type="hidden" name="section" value="workshops">
		                                        <a href="javascript:{}" onclick="document.getElementById('workshops').submit();" ><i class="zmdi zmdi-view-list"></i> <span> Talleres </span> </a> 
		                                         &nbsp;&nbsp;&nbsp;&nbsp;
		                                    </form>&nbsp;
			                            </li>

			                            <li class="has-submenu ">
			                                <form method="POST" id="reports" action="admin.php">
		                                        <input type="hidden" name="section" value="reports">
		                                        <a href="javascript:{}" onclick="document.getElementById('reports').submit();" ><i class="zmdi zmdi-view-list"></i> <span> Reportes </span> </a> 
		                                         &nbsp;&nbsp;&nbsp;&nbsp;
		                                    </form>&nbsp;
			                            </li>
			                        </ul>
			                        <!-- End navigation menu  -->
			                    </div>
			                </div>
			            </div>
			        </header>
                <?php }?>
            </div>
            <script type="text/javascript"> var mainUrl = "<?php echo $mainUrl; ?>"; </script>

            
        </header>
        <!-- End Navigation Bar-->