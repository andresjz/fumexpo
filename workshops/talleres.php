<?php 
$request = "http://it-eventsapi.azurewebsites.net/api/Event";
$api = file_get_contents($request); 
$eventos = json_decode($api);
echo $eventos;
?>
        <div class="wrapper">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Talleres</h4>

                            <table id="datatable-buttons" class="table table-striped table-bordered">
								<thead>
                                    <tr>
                                        <th>Nombre del Taller</th>
                                        <th>Inicio</th>
                                        <th>Fin</th>
                                        <th>Descripción</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>Taller "como entrenar a tu máscota"</td>
                                        <td> 3 JULIO 2:00 PM</td>
                                        <td> 3 JULIO 4:00 PM</td>
                                        <td> Impartida por la Veterinaria Mónica G</td>
                                    </tr>
                                    <tr>
                                        <td>Taller "Emprende un nuevo camino"</td>
                                        <td> 3 JULIO 6:00 PM</td>
                                        <td> 3 JULIO 8:00 PM</td>
                                        <td> Muchas veces no es fácil cuando ...</td>
                                    </tr>
                                    <tr>
                                        <td>Taller 4</td>
                                        <td> 3 JULIO 2:00 PM</td>
                                        <td> 3 JULIO 4:00 PM</td>
                                        <td> Dirigida por el Lic P.</td>
                                    </tr>
                                </tbody>
							</table>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->

                <!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Adminto.
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">About</a>
                                    </li>
                                    <li>
                                        <a href="#">Help</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->

            </div>
            <!-- end container -->

        </div>

<?php 
    sqlsrv_free_stmt( $getProducts);
  //  }      
?>
