<?php
include 'config.php';
require ("funciones.php");

seguridadIndex();

$error = 0;
$registrar=0;

if(isset($_POST['registrar']))
{    
    $registrar = 1;
    $error = registrarUsuario(limpiar($_POST['user']), $_POST['pass']);
   
}
else if(isset($_POST['login']))
{
    //echo "ernjrenuervouheruohfruefeurhniercn";
    $recordarme=0;
    if(isset($_POST['recordarme']))$recordarme=1;
    $error = login(limpiar($_POST['user']), $_POST['pass'],$recordarme);
    if($error>0)
    {
        header("Location: dashboard.php");
        exit();
    }
    
}
?>
<!DOCTYPE html>
<html ng-app="indexEventos">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App title -->
        <title>FUMEXPO 2016</title>

        <!-- App CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>
        <script type="text/javascript"> var mainUrl = "<?php echo $mainUrl; ?>"; </script>
    </head>
    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page" ng-controller="administrador">
            <div class="text-center">
                <!--a href="index.html" class="logo"><span>FUMEXPO<span>2016</span></span></a-->
                <img src="assets/images/fumexpo/FMX_Logo.png" width="400px" alt="logo">
                <h5 class="text-muted m-t-0 font-600">Plataforma de participantes</h5>
            </div>
            <div class="m-t-40 card-box">
                <div class="text-center">
                    <h4 class="text-uppercase font-bold m-b-0">Iniciar sesión</h4>
                </div>
                <div class="panel-body">
                            
                    <form class="form-horizontal m-t-20" name="logForm" method="POST" novalidate>

                        <div class="form-group ">
                            <div class="col-xs-12">
                                <span class="requiredA" ng-show="logForm.user.$error.required && !logForm.user.$pristine" >Debe escribir un correo electronico valido</span>
                                <input class="form-control" type="email" placeholder="Email" name="user" ng-model="user"
                                required data-ng-class="{ error: logForm.user.$invalid && !logForm.user.$pristine}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <span class="requiredA" ng-show="logForm.pass.$error.required && !logForm.pass.$pristine" >Es  necesario escribir la contraseña para poden iniciar sesión</span>
                                <input class="form-control" type="password" required="" placeholder="Contraseña" name="pass" ng-model="pass"
                                required data-ng-class="{ error: logForm.pass.$invalid && !logForm.pass.$pristine}">
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="col-xs-12">
                                <div class="checkbox checkbox-custom">
                                    <input id="checkbox-signup" type="checkbox">
                                    <label for="checkbox-signup">
                                        Recordarme
                                    </label>
                                </div>

                            </div>
                        </div>

                        <div class="form-group text-center m-t-30">
                            <div class="col-xs-12">
                                <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit" name="login" ng-disabled="logForm.$invalid || estadoMensaje == 4" ng-click="login()">Iniciar sesión</button>
                                <!-- onclick="javascript:location.href='http://events-mex.azurewebsites.net/dashboard.php'"-->
                            </div>
                        </div>

                        <div class="form-group m-t-30 m-b-0">
                            <div class="col-sm-12">
                                <a href="recover.php" class="text-muted"><i class="fa fa-lock m-r-5"></i> Recuperar contraseña</a>
                            </div>
                        </div>
                        
                    </form>

                </div>
            </div>
            <!-- end card-box-->
            

            <div class="row">
                <div class="col-sm-12 text-center">
                    <p class="text-muted">¿Aún no tienes una cuenta? <a href="register.php" class="text-primary m-l-5"><b>Registrate aquí</b></a></p>
                </div>
            </div>
            
        </div>
        <!-- end wrapper page -->
        

        
        <script>
            var resizefunc = [];
        </script>

        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
        <script src="controller.js"></script>
        <!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script--> 

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
    
    </body>
</html>