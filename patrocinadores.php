<?php 
$tsql = "SELECT * FROM dbo.attendees";
 //Executes the query
$getProducts = sqlsrv_query($conn, $tsql);
?>
        <div class="wrapper">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Patrocinadores</h4>

                            <table id="datatable-buttons" class="table table-striped table-bordered">
								<thead>
                                    <tr>
                                        <th>Nombre del Patrocinador</th>
                                        <th>Foto</th>
                                        <th>Vista Previa</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>Mausoleum</td>
                                        <td>mausoleum.png</td>
                                        <td><img src="http://www.mausoleum.com.mx/images/generales/img_logoGrande.png" width="80px"></td>
                                    </tr>

                                    <tr>
                                        <td>Industrias Alternativas</td>
                                        <td>mausoleum.png</td>
                                        <td><img src="http://www.mx.all.biz/img/mx/pred/logo/145.png" width="80px"></td>
                                    </tr>
                                    
                                </tbody>
							</table>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->

                <!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Adminto.
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">About</a>
                                    </li>
                                    <li>
                                        <a href="#">Help</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->

            </div>
            <!-- end container -->

        </div>

<?php 
    sqlsrv_free_stmt( $getProducts);
  //  }      
?>
