<!DOCTYPE html>
<html ng-app="indexEventos">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App title -->
        <title>FUMEXPO 2016</title>

        <!-- App CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>
        <style type="text/css">
        .requiredA{
            color: red;
            font-size: 12px;
        }
        </style>

    </head>
    <body>

        <!-- Navigation Bar-->
        <?php include("header.php");?>


        <div class="wrapper" ng-controller="administrador">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Registro de participantes</h4>
                    </div>
                </div>



                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="card-box p-b-0">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Registro</h4>

                            <div id="btnwizard" class="pull-in">
                                <ul>
                                    <li><a href="#tab12" data-toggle="tab">Datos generales</a></li>
                                    <li><a href="#tab22" data-toggle="tab">Empresa</a></li>
                                    <li><a href="#tab32" data-toggle="tab">Información adicional</a></li>
                                </ul>
								<form name="regForm" method="POST" novalidate>
                                <div class="tab-content m-b-0 b-0">   
									<div class="tab-pane m-t-10 fade" id="tab12">
                                        <div class="row">
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="email">Correo electrónico</label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="regForm.email.$error.required && !regForm.email.$pristine" >Es necesario escribir un correo electronico valido</span>
                                                    <input class="form-control required" id="email" name="email" ng-model="email" type="email"
                                                    required data-ng-class="{ error: regForm.email.$invalid && !regForm.email.$pristine}">
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                 <span class="requiredA" ng-show="regForm.password.$error.required && !regForm.password.$pristine" >Es necesario escribir una contraseña segura</span>
                                                <label class="col-lg-2 control-label " for="password2">Contraseña</label>
                                                <div class="col-lg-10">
                                                    <input id="password" name="password" type="password" ng-model="password" class="required form-control"
                                                    required data-ng-class="{ error: regForm.password.$invalid && !regForm.password.$pristine}" >

                                                </div>
                                            </div>

                                            <!--div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="confirm2">Confirmar contraseña</label>
                                                <div class="col-lg-10">
                                                    <input id="confirm2" name="confirm" type="text" class="required form-control">
                                                </div>
                                            </div-->
                                            
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label" for="name2"> Nombre(s)</label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="regForm.names.$error.required && !regForm.names.$pristine" >Es necesario escribir su nombre correctamente</span>
                                                    <input id="names" name="names" type="text" ng-model="names" class="required form-control" 
                                                    required data-ng-class="{ error: regForm.names.$invalid && !regForm.names.$pristine}" capitalize>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2  control-label " for="surname2"> Apellidos</label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="regForm.surname.$error.required && !regForm.surname.$pristine" >Es necesario al menos un apellido</span>
                                                    <input id="surname" name="surname" type="text" ng-model="surname" class="required form-control"
                                                    required data-ng-class="{ error: regForm.surname.$invalid && !regForm.surname.$pristine}" capitalize>

                                                </div>
                                            </div>

                                            <div class="form-group clearfix">
                                                <label class="col-lg-2  control-label " for="surname2"> Invitado por</label>
                                                <div class="col-lg-10">
                                                    <!-- <span class="requiredA" ng-show="regForm.surname.$error.required && !regForm.surname.$pristine" >Es necesario al menos un apellido</span> -->
                                                    <input id="invited_by" name="invited_by" type="text" ng-model="invited_by" class="required form-control" capitalize>
                                                </div>
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
                                    
									<div class="tab-pane m-t-10 fade" id="tab22">
                                        <div class="row">
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label" for="name2"> Empresa </label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="regForm.company.$error.required && !regForm.company.$pristine" >Es necesario el nombre de la empresa en que usted trabaja</span>
                                                    <input id="company" name="company" type="text" ng-model="company" class="required form-control"
                                                    required data-ng-class="{ error: regForm.company.$invalid && !regForm.company.$pristine}" capitalize>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="country"> País </label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="regForm.country.$error.required && !regForm.country.$pristine" >Es necesario Escribir el país del que nos visita</span>
                                                    <input id="country" name="country" type="text" ng-model="country" class="required form-control" 
                                                    required data-ng-class="{ error: regForm.country.$invalid && !regForm.country.$pristine}" capitalize>

                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="state"> Estado </label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="regForm.state.$error.required && !regForm.state.$pristine" >Es necesario poner el estado del que nos visita</span>
                                                    <input id="state" name="state" type="text" ng-model="state" class="required form-control"
                                                    required data-ng-class="{ error: regForm.state.$invalid && !regForm.state.$pristine}" capitalize>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="city"> Ciudad </label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="regForm.city.$error.required && !regForm.city.$pristine" >Es necesario poner la ciudad de la que nos visita</span>
                                                    <input id="city" name="city" type="text" ng-model="city" class="required form-control"
                                                    required data-ng-class="{ error: regForm.city.$invalid && !regForm.city.$pristine}" capitalize>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="phone"> Teléfono </label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="regForm.phone.$error.required && !regForm.phone.$pristine" >Es necesario al menos un tlefono de oficina</span>
                                                    <input id="phone" name="phone" type="text" ng-model="phone" class="required form-control"
                                                    required data-ng-class="{ error: regForm.phone.$invalid && !regForm.phone.$pristine}" capitalize>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="celular"> Celular </label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="regForm.celular.$error.required && !regForm.celular.$pristine" >Es necesario al menos telefono celular</span>
                                                    <input id="celular" name="celular" type="text"  ng-model="celular" class="required form-control"
                                                    required data-ng-class="{ error: regForm.celular.$invalid && !regForm.celular.$pristine}" capitalize>
                                                </div>
                                            </div>
                                            
                                            <!--div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="surname2"> Puesto del visitante </label>
                                                <div class="col-lg-10">
                                                    <input id="surname2" name="extra" type="text" class="required form-control">
                                                </div>
                                            </div-->

                    
                                        </div>
                                    </div>
                                    
									<div class="tab-pane m-t-10 fade" id="tab32">
                                        <div class="row">
                                            <div class="form-group clearfix">
                                                <div class="col-md-3">Tipo de pase</div>
                                                <div class="col-md-9">
                                                    <span class="requiredA" ng-show="regForm.pass.$error.required && !regForm.pass.$pristine" >Es necesario seleccionar un tipo de pase</span>
                                                    <select class="form-control" name="pass" id="pass" ng-model="pass"
                                                    required data-ng-class="{ error: regForm.pass.$invalid && !regForm.pass.$pristine}" capitalize>
                                                    <option value="">---</option>
                                                    <option value="8950" >A - $8950.00</option>
                                                    <option value="3200" >B - $3200.00</option>
                                                    <option value="150" >C - $150.00</option>
                                                    </select>

                                                </div>
                                                
                                            </div>
                                            
                                            <div class="form-group clearfix" ng-show="pass == 8950">
                                                <div class="col-md-3">Pase adicional a la fiesta blanca (Aplica Sólo Pase A)</div>
                                                <div class="col-md-9">
                                                    <span class="requiredA" ng-show="regForm.extra.$error.required || !regForm.extra.$pristine" >Si desea pases adicionales a la fiesta blanca seleccione la cantidad</span>
                                                    <select class="form-control" name="extra" id="extra" ng-model="extra"
                                                    data-ng-class="{ error: regForm.extra.$invalid && !regForm.extra.$pristine}" capitalize>
                                                    <option value="">---</option>
                                                    <option value="0">Ninguno</option>
                                                    <option value="950">1 - $ 950.00</option>
                                                    <option value="1900">2 - $1900.00</option>
                                                    </select>
                                                </div>
                                                
                                            </div>
                                            <a href="http://www.fumexpo.com/cuotas-y-formas-de-pago/" target="_blank">Más información</a>
                                            
                                                <div class="form-group clearfix">
                                                    <div class="col-md-12">
                                                    <div class="checkbox checkbox-primary">
                                                        <input id="checkbox-h2" type="checkbox" checked="">
                                                        <label for="checkbox-h2">
                                                            <a href="#">Acepto los términos y condiciones.</a>
                                                        </label>
                                                            <button class="btn btn-primary" type="button" ng-disabled="regForm.$invalid || estadoMensaje == 4" ng-click="add_users()">Registrarse</button>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                
								</div>
								</form>
                                    <div class="m-t-20"></div>

                                    <div class="pull-right">
                                        <input type='button' class='btn btn-primary button-next' name='next' value='Siguiente'/>
                                    </div>
                                    <div class="pull-left">
                                        <input type='button' class='btn btn-primary button-previous' name='previous'
                                               value='Anterior'/>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end col -->

                </div>
                <!-- end row -->


                <!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">FUMEXPO 2016</a>
                                    </li>
                                    <li>
                                        <a href="#">Soporte</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->

            </div>
            <!-- end container -->


        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

        <script src="//monospaced.github.io/bower-qrcode-generator/js/qrcode.js"></script>
        <script src="//monospaced.github.io/bower-qrcode-generator/js/qrcode_UTF8.js"></script>
        <script src="assets/plugins/qr/angular-qrcode.js"></script>
        
        <script src="controller.js"></script>
        <!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script--> 

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>

        <!-- Form wizard -->
        <script src="assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
        <script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#basicwizard').bootstrapWizard({'tabClass': 'nav nav-tabs navtab-wizard nav-justified bg-muted'});

                $('#progressbarwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index+1;
                    var $percent = ($current/$total) * 100;
                    $('#progressbarwizard').find('.bar').css({width:$percent+'%'});
                },
                'tabClass': 'nav nav-tabs navtab-wizard nav-justified bg-muted'});

                $('#btnwizard').bootstrapWizard({'tabClass': 'nav nav-tabs navtab-wizard nav-justified bg-muted','nextSelector': '.button-next', 'previousSelector': '.button-previous', 'firstSelector': '.button-first', 'lastSelector': '.button-last'});

                var $validator = $("#commentForm").validate({
                    rules: {
                        emailfield: {
                            required: true,
                            email: true,
                            minlength: 3
                        },
                        namefield: {
                            required: true,
                            minlength: 3
                        },
                        urlfield: {
                            required: true,
                            minlength: 3,
                            url: true
                        }
                    }
                });

                $('#rootwizard').bootstrapWizard({
                    'tabClass': 'nav nav-tabs navtab-wizard nav-justified bg-muted',
                    'onNext': function (tab, navigation, index) {
                        var $valid = $("#commentForm").valid();
                        if (!$valid) {
                            $validator.focusInvalid();
                            return false;
                        }
                    }
                });
            });

        </script>


    </body>
</html>