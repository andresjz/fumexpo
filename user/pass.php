        <?php 
            
            
            if(isset($_GET['id'])){
                $idUsuario = $_GET['id'];
            }else{
                $idUsuario = $_SESSION['usuario'];
            }
        ?>

        <div class="wrapper" ng-controller="administrador">
            <div class="container">

                <!-- Page-Title -->
                <div class="row" ng-init="edit_users('<?php echo $idUsuario; ?>')">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <!--button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul-->
                            <a href="javascript:window.print(); void 0;" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i></a>

                        </div>
                        <h4 class="page-title">Pase de acceso</h4>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <!-- <div class="panel-heading">
                                <h4>Invoice</h4>
                            </div> -->
                            <div class="panel-body">
                                <div class="clearfix">
                                    <div class="pull-left">
                                        <img src="assets/images/fumexpo/FMX_Logo.png" width="200px;">
                                    </div>
                                    <div class="col-md-8">
                                        <h1 class="invoice-logo" align="center" style="color:#000;">FUMEXPO 2016</h1>
                                    </div>
                                    <div class="pull-right">
                                        <h4>Pase # <br>
                                            <strong><?php echo $idUsuario; ?></strong>
                                        </h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left m-t-30">
                                            <h2>FUMEXPO 2016</h2>
                                            <p class="m-t-10">Entrada general <span class="label label-pink">{{passName}}</span></p>
                                            <hr>
                                            <h2>{{names}} {{surname}}</h2>
                                            <p class="m-t-10">Participante</p>
                                            <hr>
                                            <div ng-repeat="actividad in events[0].activities">{{actividad.name}}</div> 
                                            <p class="m-t-10">Actividades</p>

                                        </div>
                                        <div class="pull-left m-t-30">
                                            <h2 ng-show="passName == 'PASE TIPO A'" >19 de Octubre de 2016 </h2>
                                            <h2 ng-show="passName == 'PASE TIPO B'" >20 de Octubre de 2016 </h2>
                                            <h2 ng-show="passName == 'PASE TIPO C'" >21 de Octubre de 2016 </h2>
                                            <p class="m-t-10">10:00 AM </p>
                                            <hr>
                                        </div>
                                        <div class="pull-right m-t-30">
                                            <address>
                                              <strong>Centro Expositor y de Convenciones Puebla</strong><br>
                                               Ejército Oriente No. 100<br> Zona de los Fuertes<br> 72260 Puebla, Pue.<br>
                                              <abbr title="Phone"></abbr> +52 - 222 - 403 8877 <br>
                                              <abbr title="Phone"></abbr> +52 - 222 - 403 8889 
                                              </address>
                                        </div>

                                    </div><!-- end col -->
                                </div>
                                <!-- end row -->
                                <hr>

                                                                
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <div class="clearfix m-t-40">
                                            <h5 class="small text-inverse font-600">Información adicional</h5>

                                            <small>
                                                No olvide llevar su pase ya sea este impreso o simplemente presente su código QR al ingresar al evento
                                            </small>
                                        </div>
                                    </div>
                                    <div class="col-md-3  col-md-offset-3">
                                        <!-- <img src="assets/images/qr.png" width="100px;" align="right"> -->
                                        <?php 
                                            //$_SESSION["usuario"]= "sjdfhjksdfh283789723d";
                                            
                                            // $passUser = "http://fumexpo.azurewebsites.net/confirm/index.php?user=".$idUsuario;
                                            $passUser = $idUsuario;
                                            // echo $passUser;

                                        ?>
                                        <qrcode version="5" error-correction-level="L" size="150" data="<?php echo $passUser; ?>" download></qrcode>
                                    </div>
                                </div>
                                <hr>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- end row -->


        

            </div>
            <!-- end container -->

        </div>