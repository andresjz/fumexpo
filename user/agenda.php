    <div class="wrapper" ng-controller="workshops">
            <div class="container" ng-init="get_agenda()">

                <!-- Page-Title -->
                <div class="row" ng-init="get_workshops()">
                    <!-- <div class="col-sm-12" ng-init="edit_users('bacfbe95-8a8f-4c27-a7af-a440da821239')"> -->
                    <div class="col-sm-12" ng-init="edit_users('<?php echo $_SESSION['usuario']; ?>')">
                        <div class="btn-group pull-right m-t-15">
                            <!-- <button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul> -->
                        </div>
                        <h4 class="page-title">Agenda</h4>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <img src="agenda/21.jpg" class="img-responsive">
                    </div>
                    <div class="col-sm-6">
                        <img src="agenda/22.jpg" class="img-responsive">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <?php if ($_SESSION["pass"] == "PASETIPOA"  || $_SESSION["role"] == "Administrador") { ?>
                        <div class="col-sm-12" ng-show="payment == false && events == null"> 
                            Aún no puedes elegir actividades en tu agenda, esto debe a dos posibles razones:
                            <br> - Aún no has llevado a cabo el pago 
                            <br> - Aún no se ha autorizado tu pago. 
                            (Se paciente y espera un poco más en lo que nos encargamos de validarlo)
                        </div>

                        <!-- <div class="col-sm-12" ng-show="payment == true && events == null"> 
                            <h1>  A partir del 1 de septiembre podrás seleccionar talleres y tours en los que desees participar.</h1>
                        </div> -->
                        <?php } ?>
                        <!-- <div class="timeline" ng-repeat="activity in pagedAgenda | orderBy : 'starting_date'" ng-init="diaHoy = (activity.starting_date | limitTo : 2 : 8)" > -->
                        <div class="timeline" ng-repeat="activity in pagedAgenda | orderBy : 'starting_date'" ng-init="sectionIndex = $index" >
                          <!-- {{sectionIndex}} -->
                        <?php if ($_SESSION["pass"] == "PASETIPOA"  || $_SESSION["role"] == "Administrador") { ?>
                            <!-- <article ng-show="$index == 0 " class="timeline-item alt" > -->
                            <article ng-show="$index == 0  && events == null  && payment == true" class="timeline-item alt" >
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow-alt"></span>
                                            <span class="timeline-icon bg-danger"><i class="zmdi zmdi-circle"></i></span>
                                            *Tu pase tipo A te da acceso a 3 talleres y un tour, a continuación debes elegir cada una de estas actividades y presionar el botón "Guardar Actividades"
                                            <br>**Recuerda una vez presionado no podrás volver a editarlo
                                            <h4 class="text-danger">Registro de tours y talleres</h4>
                                            <p class="timeline-date text-muted"><small>{{activity.starting_date | date : 'shortTime' : 'UTC'}} - {{activity.ending_date | date : 'shortTime' : '0000'}} </small></p>
                                            <div>
                                                    <form name="register_work" id="register_work" method="POST" novalidate>
                                                    <p>Talleres</p>
                                                    <h3>Actividad 1</h3>
                                                    <select class="form-control" ng-model="worksh" name="worksh" id="worksh" onchange="mostrar()">
                                                        <option ng-repeat="workshop in activity.activities | orderBy : 'start' track by $index "   ng-if="workshop" value="{{workshop}}"  ng-show="workshop.speaker == 'Taller' && workshop.quantity > 0 &&  workshop!= null">{{workshop.start | date:"d / M / yy h:mm a" : "UTC" }} {{workshop.end | date:"   h:mm a" : "UTC" }} - {{workshop.name}}</option>
                                                    </select>
                                                    <div id="act1"></div>
                                                    <h3>Actividad 2</h3>
                                                    <select class="form-control" ng-model="worksh1" name="worksh1" id="worksh1" onchange="mostrar()">
                                                        <option ng-repeat="workshop in activity.activities | orderBy : 'start' track by $index "   ng-if="workshop" value="{{workshop}}"  ng-show="workshop.speaker == 'Taller' && workshop.quantity > 0 &&  workshop!= null">{{workshop.start | date:"d / M / yy   h:mm a" : "UTC" }}  {{workshop.end | date:"   h:mm a" : "UTC" }} ---  {{workshop.name}}</option>
                                                    </select>
                                                    <div id="act2"></div>
                                                     <h3>Actividad 3</h3>
                                                    <select class="form-control" ng-model="worksh2" name="worksh2" id="worksh2" onchange="mostrar()">
                                                        <option ng-repeat="workshop in activity.activities | orderBy : 'start' track by $index "   ng-if="workshop" value="{{workshop}}"  ng-show="workshop.speaker == 'Taller' && workshop.quantity > 0 && workshop!= null">{{workshop.start | date:"d / M / yy  h:mm a" : "UTC" }} - {{workshop.end | date:"   h:mm a" : "UTC" }} --- {{workshop.name}}</option>
                                                    </select>
                                                    <div id="act3"></div>
                                                    <h3>Tour </h3>
                                                    <select class="form-control" ng-model="tour" name="tour" id="tour" onchange="mostrar()" required>
                                                        <option ng-repeat="workshop in activity.activities | orderBy : 'start' track by $index "  ng-if="workshop" value="{{workshop}}" ng-show="workshop.speaker == 'Tour' && workshop.quantity > 0 &&  workshop!= null">{{workshop.start | date:"d / M / yy  h:mm a" : "UTC" }} - {{workshop.end | date:"   h:mm a" : "UTC" }} ---  {{workshop.name}}</option>
                                                    </select>
                                                    <div id="act4"></div>
                                                    <br>
                                                    <!-- <button class="btn btn-primary" ng-click="register_workshops('bacfbe95-8a8f-4c27-a7af-a440da821239')"> Guardar Talleres</button> -->
                                                    <button class="btn btn-primary" ng-disabled="register_work.$invalid || worksh1 == worksh2 || worksh1 == worksh || worksh2 == worksh " ng-click="register_workshops('<?php echo $_SESSION['usuario']; ?>')"> Guardar Talleres</button>
                                                    <script type="text/javascript">
                                                       function mostrar () {
                                                            
                                                           var actM1 = document.getElementById("worksh").value;
                                                           var actM2 = document.getElementById("worksh1").value;
                                                           var actM3 = document.getElementById("worksh2").value;
                                                           var actM4 = document.getElementById("tour").value;

                                                           actM1 = JSON.parse(actM1);
                                                           actM2 = JSON.parse(actM2);
                                                           actM3 = JSON.parse(actM3);
                                                           actM4 = JSON.parse(actM4);

                                                           
                                                           //actM1 = actM1.replace('","', '<br>'); 
                                                           // document.getElementById("act1").innerHTML = "Nombre: <br>"+ actM1.name +"<br>Horario:" + actM1.start + ""+actM1.end +"<br>Cupo:" + actM1.quantity + "<br>Descripción:" + actM1.description + "<hr>";
                                                           // document.getElementById("act2").innerHTML = "Nombre: <br>"+ actM2.name +"<br>Horario:" + actM2.start + ""+actM1.end +"<br>Cupo:" + actM1.quantity + "<br>Descripción:" + actM2.description + "<hr>";
                                                           // document.getElementById("act3").innerHTML = "Nombre: <br>"+ actM3.name +"<br>Horario:" + actM3.start + ""+actM1.end +"<br>Cupo:" + actM1.quantity + "<br>Descripción:" + actM3.description + "<hr>";
                                                           // document.getElementById("act4").innerHTML = "Nombre: <br>"+ actM4.name +"<br>Horario:" + actM4.start + ""+actM1.end +"<br>Cupo:" + actM1.quantity + "<br>Descripción:" + actM4.description + "<hr>";

                                                           document.getElementById("act1").innerHTML = "Nombre: <br> <strong>"+ actM1.name +"</strong><br>Lugares disponibles: <strong>" + actM1.quantity + "</strong><br>Descripción: " + actM1.description + "<hr>";
                                                           document.getElementById("act2").innerHTML = "Nombre: <br> <strong>"+ actM2.name +"</strong><br>Lugares disponibles: <strong>" + actM2.quantity + "</strong><br>Descripción: " + actM2.description + "<hr>";
                                                           document.getElementById("act3").innerHTML = "Nombre: <br> <strong>"+ actM3.name +"</strong><br>Lugares disponibles: <strong>" + actM3.quantity + "</strong><br>Descripción: " + actM3.description + "<hr>";
                                                           document.getElementById("act4").innerHTML = "Nombre: <br> <strong>"+ actM4.name +"</strong><br>Lugares disponibles: <strong>" + actM4.quantity + "</strong><br>Descripción: " + actM4.description + "<hr>";

                                                       }
                                                    </script>
                                                    
                                                    
                                                   
                                                    
                                                    </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article ng-show="$index == 0  && events != null" class="timeline-item alt" >
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow-alt"></span>
                                            <span class="timeline-icon bg-danger"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-danger">Registro de tours y talleres</h4>
                                            <p class="timeline-date text-muted"><small>{{activity.starting_date | date : 'shortTime' : 'UTC'}} - {{activity.ending_date | date : 'shortTime' : '0000'}} </small></p>
                                            <div>
                                                <div ng-repeat="actividad in events[0].activities">{{actividad.name}}</div>    
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        <?php } ?>
                            
                             <!--         <article class="timeline-item ">
                                <div class="text-right">
                                    <div class="time-show">
                                        <a href="#" class="btn btn-custom w-lg">Yesterday</a>
                                    </div>
                                </div>
                            </article> -->
                            <article class="col-sm-2 col-sm-offset-4 timeline-item"ng-show="activity.name == 'Coffe Break' || activity.name == 'Comida' " >
                                <div class="text-right">
                                    <div class="time-show">
                                        <a href="#" style="background: #000;" class="btn btn-custom w-lg">{{activity.starting_date | date : 'shortTime' : 'UTC'}} - {{activity.ending_date | date : 'shortTime' : 'UTC'}} <br>{{activity.name}}</a>
                                    </div>
                                </div>
                            </article>

                            <article class="col-sm-2 col-sm-offset-4 timeline-item" ng-show="sectionIndex == 0 || sectionIndex == 8 || sectionIndex == 12">
                                <div class="text-right">
                                    <div class="time-show">
                                        <!-- <a href="#" style="background: #000;" class="btn btn-custom w-lg">{{ diaHoy }} de octubre de 2016<br></a> -->
                                        <strong><a href="#" style="background: #E95200; font-size: 24px;" class="btn btn-custom w-lg">{{ activity.starting_date | date : 'dd' : 'UTC'}} de octubre de 2016</a></strong>
                                    </div>
                                </div>
                            </article>

                            <article ng-show="$index%2 == 0 && $index > 0 && activity.name != 'Coffe Break'  && activity.name != 'Comida'" class="timeline-item alt" >
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow-alt"></span>
                                            <span class="timeline-icon bg-danger"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-danger">{{activity.name}}</h4>
                                            <p class="timeline-date text-muted"><small>{{activity.starting_date | date : 'shortTime' : 'UTC'}} - {{activity.ending_date | date : 'shortTime' : 'UTC'}} </small></p>
                                            <p>{{activity.activities[0].name}}</p>
                                        </div>
                                    </div>
                                </div>
                            </article>

                            
                            <article ng-show="$index%2 != 0  && activity.name != 'Coffe Break' && activity.name != 'Comida' " class="timeline-item ">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow"></span>
                                            <span class="timeline-icon bg-success"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-success">{{activity.name}}</h4>
                                            <p class="timeline-date text-muted"><small>{{activity.starting_date | date : 'shortTime' : 'UTC'}} - {{activity.ending_date | date : 'shortTime' : 'UTC'}}</small></p>
                                            <p>{{activity.activities[0].name}}</p>

                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->

        </div>


<!--        <div class="wrapper">
            <div class="container">

                <!-- Page-Title - - >
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <!-- <button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul> - ->
                        </div>
                        <h4 class="page-title">Agenda</h4>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="timeline">
                            <article class="timeline-item alt">
                                <div class="text-right">
                                    <div class="time-show first">
                                        <a href="#" class="btn btn-custom w-lg">DÍA 1 <br>20 OCT 2016</a>
                                    </div>
                                </div>
                            </article>
                            <?php if ($_SESSION["pass"] == "PASETIPOA" || $_SESSION["pass"] == "PASETIPOB" || $_SESSION["role"] == "Administrador") { ?>
                            <article class="timeline-item alt">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow-alt"></span>
                                            <span class="timeline-icon bg-danger"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-danger">CONFERENCIA</h4>
                                            <p class="timeline-date text-muted"><small>10:00 AM - 11:30 AM </small></p>
                                            <p>Los últimos días de vida. Morir en Paz</p>
                                        </div>
                                    </div>
                                </div>
                            </article>
							<article class="timeline-item alt" >
                                <div class="text-right">
                                    <div class="time-show first">
                                        <a href="#" style="background: #000;" class="btn btn-custom w-lg">11:30 AM - 12:00 PM  <br>COFFE BREAK</a>
                                    </div>
                                </div>
                            </article>
                            <article class="timeline-item ">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow"></span>
                                            <span class="timeline-icon bg-success"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-success">PRESENTACION ANDF</h4>
                                            <p class="timeline-date text-muted"><small>12:00 PM - 12:30 PM</small></p>
                                            <p>Cómo hacer posible lo imposible </p>

                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="timeline-item alt">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow-alt"></span>
                                            <span class="timeline-icon bg-primary"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-primary">CONFERENCIA</h4>
                                            <p class="timeline-date text-muted"><small>12:30 PM - 2:00 PM</small></p>
                                            <!--p>Cómo hacer posible lo imposible</p>
                                            <!--div class="album">
                                                <a href="#">
                                                    <img alt="" src="assets/images/small/img1.jpg">
                                                </a>
                                                <a href="#">
                                                    <img alt="" src="assets/images/small/img2.jpg">
                                                </a>
                                                <a href="#">
                                                    <img alt="" src="assets/images/small/img3.jpg">
                                                </a>
                                            </div- ->
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="timeline-item alt" >
                                <div class="text-right">
                                    <div class="time-show first">
                                        <a href="#" style="background: #000;" class="btn btn-custom w-lg">2:00 PM - 4:00 PM <br>COMIDA</a>
                                    </div>
                                </div>
                            </article>
							<article class="timeline-item">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow"></span>
                                            <span class="timeline-icon bg-purple"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-purple">ACTIVIDAD DE INTEGRACIÓN</h4>
                                            <p class="timeline-date text-muted"><small>4:OO PM 7:00 PM</small></p>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="timeline-item alt">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow-alt"></span>
                                            <span class="timeline-icon"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-muted">ENTREGA DE PREMIOS</h4>
                                            <p class="timeline-date text-muted"><small>7:00 PM 8:00PM </small></p>
   
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <?php } ?>
                            <article class="timeline-item alt">
                                <div class="text-right">
                                    <div class="time-show">
                                        <a href="#" class="btn btn-custom w-lg">DÍA 2 <br> 21 OCT 2016</a>
                                    </div>
                                </div>
                            </article>
                            <?php if ($_SESSION["pass"] == "PASETIPOA" || $_SESSION["pass"] == "PASETIPOB" || $_SESSION["pass"] == "PASETIPOC" || $_SESSION["role"] == "Administrador" ) { ?>
                            <article class="timeline-item">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow"></span>
                                            <span class="timeline-icon bg-warning"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-warning">INAGURACIÓN FUMEXPO</h4>
                                            <p class="timeline-date text-muted"><small>10:00 AM - 10:15 AM</small></p>
                                            <!--p>Montly Regular Medical check up at Greenland Hospital by the
                                                doctor <span><a href="#"> Johm meon </a></span- ->
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="timeline-item alt">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow-alt"></span>
                                            <span class="timeline-icon bg-primary"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-primary">EXPO</h4>
                                            <p class="timeline-date text-muted"><small>10:15 AM - 1:00 PM</small></p>
                                            <!--p>Download the new updates of Ubold admin dashboard</p- ->
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="timeline-item">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow"></span>
                                            <span class="timeline-icon bg-success"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-success">FUNERAL CAR SHOW</h4>
                                            <p class="timeline-date text-muted"><small>1:00 PM 3:00 PM</small></p>
                                            <!--p>Jonatha Smith added new milestone <span><a class="blue" href="#">crishtian</a></span>
                                                Lorem ipsum dolor sit amet consiquest dio</p- ->
                                        </div>
                                    </div>
                                </div>
                            </article>
							<article class="timeline-item alt">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow-alt"></span>
                                            <span class="timeline-icon bg-primary"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-primary">EXPO</h4>
                                            <p class="timeline-date text-muted"><small>3:00 PM - 6:00 PM</small></p>
                                            <!--p>Download the new updates of Ubold admin dashboard</p- ->
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <?php } ?>
                            <article class="timeline-item alt">
                                <div class="text-right">
                                    <div class="time-show">
                                        <a href="#" class="btn btn-custom w-lg">DÍA 3 <br> 22 OCT 2016</a>
                                    </div>
                                </div>
                            </article>
                            <?php if ( $_SESSION["pass"] == "PASETIPOA" || $_SESSION["pass"] == "PASETIPOB" || $_SESSION["pass"] == "PASETIPOC" || $_SESSION["role"] == "Administrador" ) { ?>
                            <article class="timeline-item alt">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow-alt"></span>
                                            <span class="timeline-icon"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-muted">EXPO</h4>
                                            <p class="timeline-date text-muted"><small>10:00 AM - 11:00 AM</small></p>
                                            <!--p></p- ->
                                        </div>
                                    </div>
                                </div>
                            </article>

                            <article class="timeline-item">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow"></span>
                                            <span class="timeline-icon bg-danger"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-danger">PREMIACIÓN FUNERAL CAR SHOW</h4>
                                            <p class="timeline-date text-muted"><small>11:00 AM 11:30 AM</small></p>
                                            <!--p>Jonatha Smith added new milestone <span><a href="#">prank</a></span>
                                                Lorem ipsum dolor sit amet consiquest dio</p- ->
                                        </div>
                                    </div>
                                </div>
                            </article>
							
							<article class="timeline-item alt">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow-alt"></span>
                                            <span class="timeline-icon"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-muted">EXPO</h4>
                                            <p class="timeline-date text-muted"><small>11:30 AM - 5:00 PM</small></p>
                                            <!--p></p- ->
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <?php //} if ($_SESSION["Pass"] == "8950" ) { ?>
							<article class="timeline-item">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow"></span>
                                            <span class="timeline-icon bg-danger"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-danger">RIFA GENERAL</h4>
                                            <p class="timeline-date text-muted"><small>5:30 PM 6:00 PM</small></p>
                                            <!--p>Jonatha Smith added new milestone <span><a href="#">prank</a></span>
                                                Lorem ipsum dolor sit amet consiquest dio</p- ->
                                        </div>
                                    </div>
                                </div>
                            </article>
							
							<article class="timeline-item alt">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow-alt"></span>
                                            <span class="timeline-icon"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-muted">FIESTA BLANCA</h4>
                                            <p class="timeline-date text-muted"><small>9:00 PM - 3:00 AM</small></p>
                                            <!--p></p- ->
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <?php  } ?>

                        </div>
                    </div>
                </div>
                -->