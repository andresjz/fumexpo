        <?php 
             $idUsuario = $_SESSION['usuario'];
        ?>
        <div class="wrapper" ng-controller="administrador">
            <div class="container">
                <div class="row" ng-init="edit_users('<?php echo $idUsuario; ?>')"> 
                    <div class="col-sm-12">
                        <h3 class="page-title">¡Hola <strong><?php echo $_SESSION['nombre']; echo " "; echo $_SESSION['Surname'];?> !</strong></h3>
                    </div>
                
                
                <div class="col-lg-4">
                    
                    
                        <div class="panel panel-custom panel-border">
                            <div class="panel-heading">
                                <h2 class="panel-title"><i class="fa fa-user fa-2x"></i> Perfil</h2>
                            </div>
                            <div class="panel-body">
                                <p>
                                    En esta sección podrás consultar tu información personal, así como los datos de contacto que deseas compartir con otros participantes en el evento. 
                                    <center>
                                    <form method="POST" action="dashboard.php">
                                        <input type="hidden" name="section" value="profile">
                                        <button type="submit" class="btn btn-default btn-trans waves-effect w-md m-b-5" >Ver perfil</button> 
                                    </form>
                                    </center>

                                    
                                </p>
                            </div>
                        </div>
                </div>
                    
                <div class="col-lg-4">
                    
                    <div class="panel panel-custom panel-border">
                        <div class="panel-heading">
                                <h2 class="panel-title"><i class="fa fa-calendar fa-2x"></i> Agenda</h2>
                            </div>
                            <div class="panel-body">
                                <p>
                                    En esta sección podrás visualizar las actividades de FUMEXPO 2016, así como inscribirte a ellas o a tus colaboradores.  
                                    <br>
                                    <center>
                                    <form method="POST" action="dashboard.php">
                                        <input type="hidden" name="section" value="agenda">
                                        <button type="submit" class="btn btn-default btn-trans waves-effect w-md m-b-5" >Ver agenda</button> 
                                    </form>
                                    </center>
                                </p>
                            </div>
                        </div>
                    </div>
                     
                     
                    <div class="col-lg-4">
                        <div class="panel panel-custom panel-border">
                            <div class="panel-heading">
                                <h2 class="panel-title"><i class="fa fa-ticket fa-2x"></i> Boleto </h2>
                            </div>
                            <div class="panel-body">
                                <p>
                                    Una vez que selecciones las actividades en las que participarás, tu boleto estará disponibel en esta sección para imprimir o mostrar en dispositivos móviles.   
                                    
                                    <!-- <center disabled><a href="pass.php" class="btn btn-default btn-trans waves-effect w-md m-b-5" disabled>  Ver boleto</a></center> -->
                                    <center>
                                    <form method="POST" action="dashboard.php">
                                        <input type="hidden" name="section" value="pass">
                                        <button ng-show="payment == true" type="submit" class="btn btn-default btn-trans waves-effect w-md m-b-5">Ver boleto</button> 
                                        <button ng-show="payment == false" type="submit" class="btn btn-default btn-trans waves-effect w-md m-b-5" disabled="">Ver boleto</button> 
                                    </form>
                                    </center>
                                </p>
                            </div>
                        </div>
                    </div>
                     
                    <div class="col-lg-4">
                        <div class="panel panel-custom panel-border">
                            <div class="panel-heading">
                                <h2 class="panel-title"><i class="fa fa-ticket fa-2x"></i> Diploma </h2>
                            </div>
                            <div class="panel-body">
                                <p>
                                    Descarga aquí tu constancia de participación y asistencia de FUMEXPO2016.    
                                    
                                    <!-- <center><a href="prueba.php" class="btn btn-default btn-trans waves-effect w-md m-b-5">  Ver diplomas</a></center> -->
                                    <center><a href="certificate/index.php?user=<?php echo $idUsuario; ?>" class="btn btn-default btn-trans waves-effect w-md m-b-5">  Ver diplomas</a></center>
                                </p>
                            </div>
                        </div>
                    </div>
                     
                    <div class="col-lg-4">
                        <div class="panel panel-custom panel-border">
                            <div class="panel-heading">
                                <h2 class="panel-title"><i class="fa fa-qrcode fa-2x"></i> Networking </h2>
                            </div>
                            <div class="panel-body">
                                <p>
                                    Escanea los códigos QR de otros participantes y visualiza sus datos de contacto en esta sección.
                                    <center><button disabled type="button" class="btn btn-default btn-trans waves-effect w-md m-b-5">  Ver mis contactos</button></center>
                                </p>
                            </div>
                        </div>
                    </div>
                     
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>