/**
* Theme: Adminto Admin Template
* Author: Coderthemes
* Component: Datatable
* 
*/

var handleDataTableButtons = function () {
    "use strict";
    0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
        dom: "Bfrtip",
        buttons: [{extend: "copy", className: "btn-sm"}, {extend: "csv", className: "btn-sm"}, {
            extend: "excel",
            className: "btn-sm"
        }, {extend: "pdf", className: "btn-sm"}, {extend: "print", className: "btn-sm"}],
        scrollX: true,


       
        
       /*ajax: {
            "url": "http://it-eventsapi.azurewebsites.net/api/User/Get",
            "type": "GET"
        },
        aoColumns: [
                { "mData": "id" },
                { "mData": "firstname" },
                { "mData": "lastname" }
        ]*/
    })
}, TableManageButtons = function () {
    "use strict";
    return {
        init: function () {
            handleDataTableButtons()
        }
    }
}();

/*
var table = $('#datatable-buttons').DataTable();
 
$('#datatable-buttons tbody').on( 'click', 'img.icon-delete', function () {
    table
        .row( $(this).parents('tr') )
        .remove()
        .draw();
} ); */