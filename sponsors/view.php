<?php 
$request = "http://it-eventsapi.azurewebsites.net/api/Partner";
$api = file_get_contents($request); 
$sponsors = json_decode($api);
// echo $sponsors;
?>
        <div class="wrapper" ng-controller="sponsors">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <div class="dropdown pull-right">
                                 <a href="#" data-toggle="modal" data-target="#add" class="btn btn-default" ng-click="oper = 1">Agregar nuevo</a>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Asistentes</h4>

                            <!-- <table id="datatable-buttons" class="table table-striped table-bordered" ng-init="get_users()"> -->
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive " data-page-length='1000'>
                                <thead>
                                    <tr>
                                        <th>Nombre del Patrocinador</th>
                                        <th>sitio web</th>
                                        <th>Logo</th>
                                        <th>Teléfono</th>
                                        <th>Giro</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php 
                                    // $cont = 0;
                                    // print_r($sponsors);
                                     foreach ($sponsors as $value) {
                                    
                                    ?>
                                    
                                    <tr>
                                        <td><?php echo $value->name; ?></td>
                                        <td><a href="http://<?php echo $value->website; ?>"><?php echo $value->website; ?></a></td>
                                        <td><img src="http://<?php echo $value->logo; ?>" width="80px"></td>
                                        <td><?php echo $value->phone; ?></td>
                                        <td><?php echo $value->giro; ?></td>
                                        <td>
                                            <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5" ng-click="delete_sponsors('<?php echo $value->id; ?>')" onclick="deleteRow(this)"> <i class="fa fa-remove"></i> </button>

                                            <!-- <a href="#" data-toggle="modal" data-target="#add"  class="btn btn-icon waves-effect waves-light btn-success m-b-5"  ng-click="edit_sponsors('<?php echo $value->id; ?>')">
                                            <i class="fa fa-edit">
                                            </a> -->
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <script type="text/javascript">
                                        function deleteRow(btn) {
                                            var row = btn.parentNode.parentNode;
                                            row.parentNode.removeChild(row);
                                        }
                                </script>
                            </table>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->

                <!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Adminto.
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">About</a>
                                    </li>
                                    <li>
                                        <a href="#">Help</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->

            </div>
            <!-- end container -->

            <!-- MODAL -->
            <div class="modal fade" tabindex="-1" role="dialog" id="add">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" ng-show="oper == 1">Agregar nuevo</h4>
                    <h4 class="modal-title" ng-show="oper == 2">Editar </h4>
                  </div>
                  
                  <form class="form-horizontal" role="form" id="AddForm" name="AddForm" novalidate>
                  <div class="modal-body">
                                                        
                                                        <div class="form-group ">
                                                            <label class="col-lg-2 control-label " for="nameS">Nombre del patrocinador</label>
                                                            <div class="col-lg-10">
                                                                <span class="requiredA" ng-show="AddForm.nameS.$error.required && !AddForm.nameS.$pristine" >Es necesario escribir un nombre</span>
                                                                <input class="form-control required" id="nameS" name="nameS" ng-model="nameS" type="text"
                                                                required data-ng-class="{ error: AddForm.nameS.$invalid && !AddForm.nameS.$pristine}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group ">
                                                            <label class="col-lg-2 control-label " for="siteS">Sitio web</label>
                                                            <div class="col-lg-10">
                                                                <span class="requiredA" ng-show="AddForm.siteS.$error.required && !AddForm.siteS.$pristine" >Es necesario escribir una dirección web</span>
                                                                <input class="form-control required" id="siteS" name="siteS" ng-model="siteS" type="text"
                                                                required data-ng-class="{ error: AddForm.siteS.$invalid && !AddForm.siteS.$pristine}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group ">
                                                            <label class="col-lg-2 control-label " for="logoS">Url imagen del logo</label>
                                                            <div class="col-lg-10">
                                                                <span class="requiredA" ng-show="AddForm.logoS.$error.required && !AddForm.logoS.$pristine" >Es necesario poner un logo</span>
                                                                <input class="form-control required" id="logoS" name="logoS" ng-model="logoS" type="text"
                                                                required data-ng-class="{ error: AddForm.logoS.$invalid && !AddForm.logoS.$pristine}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group ">
                                                            <label class="col-lg-2 control-label " for="phoneS">Teléfono</label>
                                                            <div class="col-lg-10">
                                                                <span class="requiredA" ng-show="AddForm.phoneS.$error.required && !AddForm.phoneS.$pristine" >Es necesario escribir un número de teléfono</span>
                                                                <input class="form-control required" id="phoneS" name="phoneS" ng-model="phoneS" type="text"
                                                                required data-ng-class="{ error: AddForm.phoneS.$invalid && !AddForm.phoneS.$pristine}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group ">
                                                            <label class="col-lg-2 control-label " for="giroS">Giro</label>
                                                            <div class="col-lg-10">
                                                                <span class="requiredA" ng-show="AddForm.giroS.$error.required && !AddForm.giroS.$pristine" >Es necesario escribir un giro</span>
                                                                <input class="form-control required" id="giroS" name="giroS" ng-model="giroS" type="text"
                                                                required data-ng-class="{ error: AddForm.giroS.$invalid && !AddForm.giroS.$pristine}">
                                                            </div>
                                                        </div>                                                                                                                
    
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-primary" type="button" ng-show="oper == 1" ng-click="add_sponsors()" data-dismiss="modal">Agregar Patrocinador</button>
                    <button class="btn btn-primary" type="button" ng-show="oper == 2" ng-click="update_sponsors(idSponsor)">Actualizar Patrocinador</button>
                    <!-- <button class="btn btn-primary" type="button" ng-disabled="AddForm.$invalid || estadoMensaje == 4" ng-click="add_workshops()">Agregar taller</button> -->
                        </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


        </div>


