<?php 
$request = "http://it-eventsapi.azurewebsites.net/api/Event";
$api = file_get_contents($request); 
$eventos = json_decode($api);

?>
        <div class="wrapper" ng-controller="agenda">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <div class="dropdown pull-right">
                                <a href="#" data-toggle="modal" data-target="#add" class="btn btn-default" ng-click="oper = 1">Agregar nuevo</a>
                               
                                <!-- <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul> -->
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Talleres</h4>

                            <table id="datatable-buttons" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Tipo de evento</th>
                                        <th>Nombre</th>
                                        <th>Descripción</th>
                                        <th>Inicio</th>
                                        <th>Fin</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>

                                <tbody>
                                 
                                 <?php 
                                    
                                    //$eventos = array_shift($eventos);
                                    //$eventos = $eventos->activities;
                                    foreach ($eventos as $value) {
                                        if ($value != null) {
                                 ?>
                                    <tr>
                                        <td><?php echo $value->name; ?></td>
                                        <td><?php echo $value->activities[0]->name; ?></td>
                                        <td><?php echo $value->activities[0]->description; ?></td>
                                        <td><?php echo $value->starting_date; ?></td>
                                        <td><?php echo $value->ending_date; ?></td>
                                        <td>
                                            
                                            <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5" ng-click="delete_agenda('<?php echo $value->id; ?>')"> <i class="fa fa-remove" ></i> </button>
                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"  data-toggle="modal" data-target="#add" ng-click="edit_agenda(<?php echo $value->id; ?>)"> <i class="fa fa-edit"></i> </button>
                                            <!-- <form method="POST" action="admin.php">
                                                <input type="hidden" name="id" value="<?php// echo $value->id; ?>">
                                                <input type="hidden" name="section" value="edit">
                                                <button type="submit" class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-edit"></i></button>
                                            </form> -->
                                        </td>
                                    </tr>
                                <?php }
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->

                <!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Adminto.
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">About</a>
                                    </li>
                                    <li>
                                        <a href="#">Help</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->

            </div>
            <!-- end container -->
                      <!-- MODAL -->
            <div class="modal fade" tabindex="-1" role="dialog" id="add">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" ng-show="oper == 1">Agregar nuevo</h4>
                    <h4 class="modal-title" ng-show="oper == 2">Editar </h4>
                  </div>
                  
                  <form class="form-horizontal" role="form" name="AddForm" novalidate>
                  <div class="modal-body">
                                                        
                                                        <div class="form-group ">
                                                            <label class="col-lg-2 control-label " for="nameE">Nombre del evento</label>
                                                            <div class="col-lg-10">
                                                                <span class="requiredA" ng-show="AddForm.nameE.$error.required && !AddForm.nameE.$pristine" >Es necesario escribir un nombre</span>
                                                                <input class="form-control required" id="nameE" name="nameE" ng-model="nameE" type="text"
                                                                required data-ng-class="{ error: AddForm.nameE.$invalid && !AddForm.nameE.$pristine}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label class="col-lg-2 control-label " for="descriptions">Descripción</label>
                                                            <div class="col-lg-10">
                                                                <span class="requiredA" ng-show="AddForm.descriptions.$error.required && !AddForm.descriptions.$pristine" >Es necesario una breve descripción</span>
                                                                <input class="form-control required" id="descriptions" name="descriptions" ng-model="descriptions" type="text"
                                                                required data-ng-class="{ error: AddForm.descriptions.$invalid && !AddForm.descriptions.$pristine}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label class="col-lg-2 control-label " for="speaker">Nombre del Ponente</label>
                                                            <div class="col-lg-10">
                                                                <span class="requiredA" ng-show="AddForm.speaker.$error.required && !AddForm.speaker.$pristine" >Es necesario una breve descripción</span>
                                                                <input class="form-control required" id="speaker" name="speaker" ng-model="speaker" type="text"
                                                                required data-ng-class="{ error: AddForm.speaker.$invalid && !AddForm.speaker.$pristine}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label class="col-lg-2 control-label " for="certificate">Reconocimiento</label>
                                                            <div class="col-lg-10">
                                                                <span class="requiredA" ng-show="AddForm.certificate.$error.required && !AddForm.certificate.$pristine" >Es necesario seleccionar un valor</span>
                                                                <select class="form-control" name="certificate" id="certificate" ng-model="certificate"
                                                                data-ng-class="{ error: AddForm.certificate.$invalid && !AddForm.certificate.$pristine}">
                                                                <option value="true">Si </option>
                                                                <option value="false">No </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label class="col-lg-2 control-label " for="quantity">Cantidad</label>
                                                            <div class="col-lg-10">
                                                                <span class="requiredA" ng-show="AddForm.quantity.$error.required && !AddForm.quantity.$pristine" >Es necesario</span>
                                                                <input class="form-control required" id="quantity" name="quantity" ng-model="quantity" type="text"
                                                                required data-ng-class="{ error: AddForm.quantity.$invalid && !AddForm.quantity.$pristine}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label class="col-lg-2 control-label " for="initDate">Inicio</label>
                                                            <div class="col-lg-10">
                                                                <div class="col-lg-8">
                                                                    <span class="requiredA" ng-show="AddForm.initDate.$error.required && !AddForm.initDate.$pristine" >Es necesario </span>
                                                                    <input class="form-control" id="initDate" name="initDate" ng-model="initDate" type="text">
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <span class="requiredA" ng-show="AddForm.initTime.$error.required && !AddForm.initTime.$pristine" >Es necesario</span>
                                                                    <input class="form-control" id="initTime" name="initTime" ng-model="initTime" type="text">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group ">
                                                            <label class="col-lg-2 control-label " for="endDate">Fin</label>
                                                            <div class="col-lg-10">
                                                                <div class="col-lg-8">
                                                                    <span class="requiredA" ng-show="AddForm.endDate.$error.required && !AddForm.endDate.$pristine" >Es necesario </span>
                                                                    <input class="form-control" id="endDate" name="endDate" ng-model="endDate" type="text">
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <span class="requiredA" ng-show="AddForm.endTime.$error.required && !AddForm.endTime.$pristine" >Es necesario</span>
                                                                    <input class="form-control" id="endTime" name="endTime" ng-model="endTime" type="text">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- <div id="demo"></div> -->
                                                        
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-primary" type="button" ng-show="oper == 1" ng-click="add_agenda()">Agregar evento</button>
                    <button class="btn btn-primary" type="button" ng-show="oper == 2" ng-click="update_agenda()">Actualizar taller</button>
                    <!-- <button class="btn btn-primary" type="button" ng-disabled="AddForm.$invalid || estadoMensaje == 4" ng-click="add_workshops()">Agregar taller</button> -->
                        </form>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
