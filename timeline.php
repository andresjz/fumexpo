<!DOCTYPE html>
<html ng-app="indexEventos">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <title>Agenda</title>

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>


    </head>


    <body>

        <?php include("header.php");
			 // seguridad();
		?>

        <div class="wrapper"ng-controller="workshops">
            <div class="container" ng-init="get_agenda()">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <!-- <button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul> -->
                        </div>
                        <h4 class="page-title">Agenda</h4>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="timeline" ng-repeat="activity in pagedAgenda">
                           
                            <article ng-show="$index%2 == 0 && activity.name != 'Coffe Break'  && activity.name != 'Comida'" class="timeline-item alt" >
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow-alt"></span>
                                            <span class="timeline-icon bg-danger"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-danger">{{activity.name}}</h4>
                                            <p class="timeline-date text-muted"><small>10:00 AM - 11:30 AM </small></p>
                                            <p>{{activity.activities[0].name}}</p>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="timeline-item alt" ng-show="activity.name == 'Coffe Break' || activity.name == 'Comida' " >
                                <div class="text-right">
                                    <div class="time-show first">
                                        <a href="#" style="background: #000;" class="btn btn-custom w-lg">2:00 PM - 4:00 PM <br>{{activity.name}}</a>
                                    </div>
                                </div>
                            </article>
                            <article ng-show="$index%2 != 0 && activity.name != 'Coffe Break' && activity.name != 'Comida' " class="timeline-item ">
                                <div class="timeline-desk">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <span class="arrow"></span>
                                            <span class="timeline-icon bg-success"><i class="zmdi zmdi-circle"></i></span>
                                            <h4 class="text-success">{{activity.name}}</h4>
                                            <p class="timeline-date text-muted"><small>12:00 PM - 12:30 PM</small></p>
                                            <p>{{activity.activities[0].name}}</p>

                                        </div>
                                    </div>
                                </div>
                            </article>

                        </div>
                    </div>
                </div>
                <!-- end row -->

                <?php include('footer.php');?>

            </div>
            <!-- end container -->

        </div>

        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
        <script src="controller.js"></script>
        <!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script--> 


        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>


        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

    </body>
</html>