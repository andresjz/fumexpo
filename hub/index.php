<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Notificaciones</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  </head>

  <body>
    <div class="col-md-8 col-md-offset-2">
      <form class="form-horizontal" action="index.php" method="POST">
          <fieldset>

          <!-- Form Name -->
          <legend>Notificaciones</legend>

          <!-- Textarea -->
          <div class="form-group">
            <label class="col-md-4 control-label" for="textarea">Mensaje</label>
            <div class="col-md-4">                     
              <textarea class="form-control" id="textarea" name="mensaje" placeholder="Escribe aquí"></textarea>
            </div>
          </div>

          <!-- Button -->
          <div class="form-group">
            <label class="col-md-4 control-label" for="singlebutton"></label>
            <div class="col-md-4">
              <button id="singlebutton" name="singlebutton" class="btn btn-success">Enviar Notificación</button>
            </div>
          </div>

          </fieldset>
      </form>
      <?php
        if(isset($_POST['mensaje'])){
          include 'notificationHub.php';
          $mensaje = $_POST['mensaje'];
          echo $mensaje;

          $hub = new NotificationHub("Endpoint=sb://fumexpo2016.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=F+Oq90N8D4klZZBDnv12mp4pGCa4Vqr0tEEOGCAWsh4=", "fumexpo"); 

          $message = '{"data":{"message":"'.$mensaje.'"}}'; 
          $notification = new Notification("gcm", $message);
          $hub->sendNotification($notification, null);

          echo "<br><br><br>Notificación Enviada";
        }
      ?>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </body>
</html>