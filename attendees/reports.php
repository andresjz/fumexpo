<?php 
$request = "http://it-eventsapi.azurewebsites.net/api/User";
$api = file_get_contents($request); 
$participantes = json_decode($api);
//echo $participantes;
?>
        <div class="wrapper" ng-controller="administrador">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Reporte de talleres</h4>

                            <!-- <table id="datatable-buttons" class="table table-striped table-bordered" ng-init="get_users()"> -->
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive " data-page-length='1000'>
								<thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Apellidos</th>
                                        <th>Correo</th>
                                        <th>Taller</th>
                                        
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php 
                                    $cont = 0;
                                    //print_r($participantes);
                                    foreach ($participantes as $value) {
                                        $cont = $cont + 1;
                                    //echo $value["id"] . ", " . $value["firstname"] . "<br>";
                                        if($value->firstname != "Cancelado"){
                                            if($value->user->events != null){
                                    ?>
                                        
                                    <tr>
                                        <td><?php echo $cont; ?></td>
                                        <td><?php echo $value->firstname; ?></td>
                                        <td><?php echo $value->lastname; ?></td>
                                        <td><?php echo $value->user->username; ?></td>
                                        <td><?php print_r($value->user->events[0]->activities[0]->name); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $cont; ?></td>
                                        <td><?php echo $value->firstname; ?></td>
                                        <td><?php echo $value->lastname; ?></td>
                                        <td><?php echo $value->user->username; ?></td>
                                        <td><?php print_r($value->user->events[0]->activities[1]->name); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $cont; ?></td>
                                        <td><?php echo $value->firstname; ?></td>
                                        <td><?php echo $value->lastname; ?></td>
                                        <td><?php echo $value->user->username; ?></td>
                                        <td><?php print_r($value->user->events[0]->activities[2]->name); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $cont; ?></td>
                                        <td><?php echo $value->firstname; ?></td>
                                        <td><?php echo $value->lastname; ?></td>
                                        <td><?php echo $value->user->username; ?></td>
                                        <td><?php print_r($value->user->events[0]->activities[3]->name); ?></td>
                                    </tr>
                                    <?php  
                                            }
                                        }
                                    } 
                                    ?>
                                </tbody>
                                <script type="text/javascript">
                                        function deleteRow(btn) {
                                            var row = btn.parentNode.parentNode;
                                            row.parentNode.removeChild(row);
                                        }
                                </script>
							</table>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->

                <!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Adminto.
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">About</a>
                                    </li>
                                    <li>
                                        <a href="#">Help</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->

            </div>
            <!-- end container -->

        </div>


