        <?php 
            
            
            if(isset($_POST['id'])){
                $idUsuario = $_POST['id'];
            }else{
                $idUsuario = $_SESSION['usuario'];
            }
        ?>

        <div class="wrapper" ng-controller="administrador">
            <div class="container">

                <!-- Page-Title -->
                <div class="row" ng-init="edit_users('<?php echo $idUsuario; ?>')">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <h4 class="page-title">Perfil de usuario</h4>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">

                           <!--  <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div> -->

                             <p class=" m-t-0 m-b-30">A continuación podrás verrificar que tus datos estén correctos y actualizar en caso de ser necesario</p>
                            <div class="row">
                                <form class="form-horizontal" role="form" name="editForm" novalidate>
                                <div class="col-lg-6">
                                <h4 class="header-title m-t-0 m-b-30">Actualizar datos del usuario</h4>
                                    
                                    
                                       
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="email">Correo electrónico</label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="editForm.email.$error.required && !editForm.email.$pristine" >Es necesario escribir un correo electronico valido</span>
                                                    <input class="form-control required" id="email" name="email" ng-model="email" type="email"
                                                    required data-ng-class="{ error: editForm.email.$invalid && !editForm.email.$pristine}">
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                 <span class="requiredA" ng-show="editForm.password.$error.required && !editForm.password.$pristine" >Es necesario escribir una contraseña segura</span>
                                                <label class="col-lg-2 control-label " for="password2">Contraseña</label>
                                                <div class="col-lg-10">
                                                    <input id="password" name="password" type="password" ng-model="password" class="required form-control"
                                                    required data-ng-class="{ error: editForm.password.$invalid && !editForm.password.$pristine}" >

                                                </div>
                                            </div>

                                            <!--div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="confirm2">Confirmar contraseña</label>
                                                <div class="col-lg-10">
                                                    <input id="confirm2" name="confirm" type="text" class="required form-control">
                                                </div>
                                            </div-->
                                            
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label" for="name2"> Nombre(s)</label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="editForm.names.$error.required && !editForm.names.$pristine" >Es necesario escribir su nombre correctamente</span>
                                                    <input id="names" name="names" type="text" ng-model="names" class="required form-control" 
                                                    required data-ng-class="{ error: editForm.names.$invalid && !editForm.names.$pristine}" capitalize>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2  control-label " for="surname2"> Apellidos</label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="editForm.surname.$error.required && !editForm.surname.$pristine" >Es necesario al menos un apellido</span>
                                                    <input id="surname" name="surname" type="text" ng-model="surname" class="required form-control"
                                                    required data-ng-class="{ error: editForm.surname.$invalid && !editForm.surname.$pristine}" capitalize>

                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2  control-label " for="surname2"> Invitado por</label>
                                                <div class="col-lg-10">
                                                    <!-- <span class="requiredA" ng-show="regForm.surname.$error.required && !regForm.surname.$pristine" >Es necesario al menos un apellido</span> -->
                                                    <input id="invited_by" name="invited_by" type="text" ng-model="invited_by" class="required form-control" capitalize>
                                                </div>
                                            </div>
                                            <?php if ( $_SESSION["role"] == "Administrador" ) { ?>
                                            <div class="row">
                                            <div class="form-group clearfix">
                                                <div class="col-md-3">Tipo de pase</div>
                                                <div class="col-md-9">
                                                    <span class="requiredA" ng-show="editForm.pass.$error.required && !editForm.pass.$pristine" >Es necesario seleccionar un tipo de pase</span>
                                                    <select class="form-control" name="pass" id="pass" ng-model="pass"
                                                    required data-ng-class="{ error: editForm.pass.$invalid && !editForm.pass.$pristine}" capitalize>
                                                    <option value="{{pass}}" selected="">{{pass}}</option>
                                                    <option value="8950" >A - $8950.00</option>
                                                    <option value="3200" >B - $3200.00</option>
                                                    <option value="150" >C - $150.00</option>
                                                    </select>

                                                </div>
                                                
                                            </div>
                                            
                                            <div class="form-group clearfix" ng-show="pass == 8950">
                                                <div class="col-md-3">Pase adicional a la fiesta blanca (Aplica Sólo Pase A)</div>
                                                <div class="col-md-9">
                                                    <span class="requiredA" ng-show="editForm.extra.$error.required || !editForm.extra.$pristine" >Si desea pases adicionales a la fiesta blanca seleccione la cantidad</span>
                                                    <select class="form-control" name="extra" id="extra" ng-model="extra"
                                                    data-ng-class="{ error: editForm.extra.$invalid && !editForm.extra.$pristine}" capitalize>
                                                    <option value="{{extra}}" selected="">{{extra}}</option>
                                                    <option value="950">1 - $ 950.00</option>
                                                    <option value="1900">2 - $1900.00</option>
                                                    </select>
                                                </div>
                                                
                                            </div>
                                            
                                            </div>
                                            <?php } else { ?>
                                            
                                            <p><strong>Pase seleccionado:</strong> {{passName}}</p>
                                            <p><strong>Costo pase:</strong> $  {{pass}}</p>
                                            
                                            <p ng-show="extra != null"><strong>Costo Adicionales</strong> {{extra}}</p>
                                            <?php } ?>
                                    
                                        
                                </div><!-- end col -->

                                 <h4 class="header-title m-t-0 m-b-30">Actualizar datos de Empresa</h4>
                                <div class="col-lg-6">
                                    
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label" for="name2"> Empresa </label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="editForm.company.$error.required && !editForm.company.$pristine" >Es necesario el nombre de la empresa en que usted trabaja</span>
                                                    <input id="company" name="company" type="text" ng-model="company" class="required form-control"
                                                    required data-ng-class="{ error: editForm.company.$invalid && !editForm.company.$pristine}" capitalize>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="country"> País </label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="editForm.country.$error.required && !editForm.country.$pristine" >Es necesario Escribir el país del que nos visita</span>
                                                    <input id="country" name="country" type="text" ng-model="country" class="required form-control" 
                                                    required data-ng-class="{ error: editForm.country.$invalid && !editForm.country.$pristine}" capitalize>

                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="state"> Estado </label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="editForm.state.$error.required && !editForm.state.$pristine" >Es necesario poner el estado del que nos visita</span>
                                                    <input id="state" name="state" type="text" ng-model="state" class="required form-control"
                                                    required data-ng-class="{ error: editForm.state.$invalid && !editForm.state.$pristine}" capitalize>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="city"> Ciudad </label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="editForm.city.$error.required && !editForm.city.$pristine" >Es necesario poner la ciudad de la que nos visita</span>
                                                    <input id="city" name="city" type="text" ng-model="city" class="required form-control"
                                                    required data-ng-class="{ error: editForm.city.$invalid && !editForm.city.$pristine}" capitalize>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="phone"> Teléfono </label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="editForm.phone.$error.required && !editForm.phone.$pristine" >Es necesario al menos un tlefono de oficina</span>
                                                    <input id="phone" name="phone" type="text" ng-model="phone" class="required form-control"
                                                    required data-ng-class="{ error: editForm.phone.$invalid && !editForm.phone.$pristine}" capitalize>
                                                </div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label class="col-lg-2 control-label " for="celular"> Celular </label>
                                                <div class="col-lg-10">
                                                    <span class="requiredA" ng-show="editForm.celular.$error.required && !editForm.celular.$pristine" >Es necesario al menos telefono celular</span>
                                                    <input id="celular" name="celular" type="text"  ng-model="celular" class="required form-control"
                                                    required data-ng-class="{ error: editForm.celular.$invalid && !editForm.celular.$pristine}" capitalize>
                                                </div>
                                            </div>
                                            <button class="btn btn-primary" type="button" ng-disabled="editForm.$invalid || estadoMensaje == 4" ng-click="update_users('<?php echo $idUsuario; ?>')">Actualizar Perfil</button>

                                </div><!-- end col -->
                                </form>

                            </div><!-- end row -->
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->
                
            </div>
            <!-- end container -->

        </div>

 