<?php 
$request = "http://it-eventsapi.azurewebsites.net/api/User";
$api = file_get_contents($request); 
$participantes = json_decode($api);
//echo $participantes;
?>
        <div class="wrapper" ng-controller="administrador">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Asistentes</h4>

                            <!-- <table id="datatable-buttons" class="table table-striped table-bordered" ng-init="get_users()"> -->
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive " data-page-length='1000'>
								<thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Apellidos</th>
                                        <th>Correo</th>
                                        <th>Compañia</th>
                                        <th>Telefono</th>
                                        <th>Estado</th>
                                        <th>Pago</th>
                                        <th>Tipo de Pase</th>
                                        <th>Acciones</th>
                                        <th>Invitado por</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php 
                                    $cont = 0;
                                    //print_r($participantes);
                                    foreach ($participantes as $value) {
                                        $cont = $cont + 1;
                                    //echo $value["id"] . ", " . $value["firstname"] . "<br>";
                                        if($value->firstname != "Cancelado"){
                                    ?>
                                        
                                    <tr>
                                        <td><?php echo $cont; ?></td>
                                        <td><?php echo $value->firstname; ?></td>
                                        <td><?php echo $value->lastname; ?></td>
                                        <td><?php echo $value->user->username; ?></td>
                                        <td><?php echo $value->user->company->name; ?></td>
                                        <td><?php echo $value->user->company->phone; ?></td>
                                        <td><?php echo $value->user->company->state; ?></td>
                                        <td><?php if($value->user->payment == false) echo "No pagado"; else echo "Pagado"; ?></td>
                                        <td><?php echo $value->user->ticket->name; ?></td>
                                        <td>
                                            <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5" ng-click="delete_users('<?php echo $value->id; ?>')" onclick="deleteRow(this)"> <i class="fa fa-remove"></i> </button>
                                            <form method="POST" action="admin.php">
                                                <input type="hidden" name="id" value="<?php echo $value->id; ?>">
                                                <input type="hidden" name="section" value="editAttendees">
                                                <button type="submit" class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-edit"></i></button>
                                            </form>
                                            <?php if ($value->user->payment == false ) { ?>
                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5" ng-click="validate_users('<?php echo $value->id; ?>')" onClick="this.disabled=true"> <i class="fa fa-thumbs-o-up"></i></button>
                                            <?php } 
                                                $pasen   =  $value->user->ticket->name;
                                                $pasen = str_replace(" ","-",$pasen);
                                             ?>
                                            <!-- <button disabled class="btn btn-icon waves-effect waves-light btn-success m-b-5" ng-click="validate_users('<?php echo $value->id; ?>')"> <i class="fa fa-thumbs-o-up"></i></button> -->
                                            
                                            <a href="http://fumexpo.azurewebsites.net/funciones.php?mail=&name=<?php echo $value->firstname; ?>&email=<?php echo $value->user->username; ?>&pass=<?php echo $value->user->ticket->price;?>&passn=<?php echo $pasen; ?>&extra=<?php echo $value->user->ticket->additional_tickets[0]->price; ?>" class="btn btn-icon waves-effect waves-light btn-success m-b-5" onClick="this.disabled=true"> <i class="fa fa-paper-plane"></i></a>
                                            <!-- http://lsys.mx/sendmail.php?recover=&email=lajuarezs@gmail.com&id=123&nombre=luis-andres&password=we123 -->
                                            <a href="http://lsys.mx/sendmail.php?recover=&email=<?php echo $value->user->username; ?>&id=<?php echo $value->id; ?>&nombre=<?php $name = $value->firstname;  $name = str_replace(" ","-",$name); echo $name; ?>&password=<?php echo $value->user->password; ?>" class="btn btn-icon waves-effect waves-light btn-success m-b-5" onClick="this.disabled=true"> <i class="fa fa-key"></i></a>
                                        </td>
                                        <td><?php echo $value->user->invited_by; ?></td>
                                        <?php } ?>

                                        
                                    </tr>
                                       <?php  } ?>
                                </tbody>
                                <script type="text/javascript">
                                        function deleteRow(btn) {
                                            var row = btn.parentNode.parentNode;
                                            row.parentNode.removeChild(row);
                                        }
                                </script>
							</table>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->

                <!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Adminto.
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">About</a>
                                    </li>
                                    <li>
                                        <a href="#">Help</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->

            </div>
            <!-- end container -->

        </div>


