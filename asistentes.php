<?php 
$request = "http://it-eventsapi.azurewebsites.net/api/User";
$api = file_get_contents($request); 
$participantes = json_decode($api);
echo $participantes;
?>
        <div class="wrapper" ng-controller="administrador">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Asistentes</h4>

                            <!-- <table id="datatable-buttons" class="table table-striped table-bordered" ng-init="get_users()"> -->
                            <table id="datatable-buttons" class="table table-striped table-bordered" >
								<thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Apellidos</th>
                                        <th>Correo</th>
                                        <th>Compañia</th>
                                        <th>Telefono</th>
                                        <th>Tipo de Pase</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php 
                                    //print_r($participantes);
                                    foreach ($participantes as $value) {
                                    //echo $value["id"] . ", " . $value["firstname"] . "<br>";
                                    ?>
                                        
                                    <tr>
                                        <td><?php echo $value->firstname; ?></td>
                                        <td><?php echo $value->lastname; ?></td>
                                        <td><?php echo $value->user->username; ?></td>
                                        <td><?php echo $value->user->company->name; ?></td>
                                        <td><?php echo $value->user->company->phone; ?></td>
                                        <td><?php echo $value->photo; ?></td>
                                        <td>
                                            <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5" ng-click="delete_users('<?php echo $value->id; ?>')" onclick="deleteRow(this)"> <i class="fa fa-remove"></i> </button>
                                            <a class="btn btn-icon waves-effect waves-light btn-info m-b-5" href="profile.php?id=<?php echo $value->id; ?>"  > <i class="fa fa-edit"></i> </a>
                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-thumbs-o-up"></i> Validar pago</button>
                                        </td>
                                        
                                    </tr>
                                       <?php  } ?>
                                </tbody>
                                <script type="text/javascript">
                                        function deleteRow(btn) {
                                            var row = btn.parentNode.parentNode;
                                            row.parentNode.removeChild(row);
                                        }
                                </script>
							</table>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->

                <!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Adminto.
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">About</a>
                                    </li>
                                    <li>
                                        <a href="#">Help</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->

            </div>
            <!-- end container -->

        </div>


