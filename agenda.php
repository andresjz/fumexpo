
        <div class="wrapper">
            <div class="container" ng-controller="workshops">

                <div class="row">
                    <div class="col-sm-12" ng-init="get_agenda()">
                        <div class="card-box table-responsive">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Agenda</h4>

                            <table id="datatable-buttons" class="table table-striped table-bordered">
								<thead>
                                    <tr>
                                        <th>Nombre del Evento</th>
                                        <th>Inicio</th>
                                        <th>Fin</th>
                                        <th>Descripción</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>Inaguración</td>
                                        <td> 3 JULIO 2:00 PM</td>
                                        <td> 3 JULIO 4:00 PM</td>
                                        <td> Inaguración del evento dirigida por el Lic P.</td>
                                    </tr>
                                    <tr>
                                        <td>Conferencia "Vida y amor"</td>
                                        <td> 3 JULIO 6:00 PM</td>
                                        <td> 3 JULIO 8:00 PM</td>
                                        <td> El Lic Arturo M nos relatará una historia única sobre...</td>
                                    </tr>
                                    <tr>
                                        <td>Cierre del evento</td>
                                        <td> 3 JULIO 2:00 PM</td>
                                        <td> 3 JULIO 4:00 PM</td>
                                        <td> Inaguración del evento dirigida por el Lic P.</td>
                                    </tr>
                                </tbody>
							</table>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->

                <!-- Footer -->
                <footer class="footer text-right">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6">
                                2016 © Adminto.
                            </div>
                            <div class="col-xs-6">
                                <ul class="pull-right list-inline m-b-0">
                                    <li>
                                        <a href="#">About</a>
                                    </li>
                                    <li>
                                        <a href="#">Help</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer -->

            </div>
            <!-- end container -->

        </div>

